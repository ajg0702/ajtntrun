package us.ajg0702.tntrun.signs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.Messages;
import us.ajg0702.tntrun.Arena.Arena;
import us.ajg0702.utils.spigot.Config;

public class ArenaSign {
	
	Messages msgs = Main.getPlugin(Main.class).msgs;
	Config config = Main.getPlugin(Main.class).config;
	
	public Arena arena;
	public Location sign;
	
	
	
	public ArenaSign(Arena ar, Location loc) {
		arena = ar;
		sign = loc;

	}
	
	public boolean update() {
		Block block = sign.getBlock();
		if(!(block.getState() instanceof Sign)) {
			int x = block.getX();
			int y = block.getY();
			int z = block.getZ();
			Bukkit.getLogger().warning("[ajTNTRun] Sign block at "+x+", "+y+", "+z+" is not a sign! Removing that block from the sign list.");
			return false;
		}
		
		Sign sign = (Sign) block.getState();
		for(int i = 0; i <4; i++) {
			//Bukkit.getLogger().info("signline: "+i);
			sign.setLine(i, getLine(i+1));
		}
		sign.update();
		return true;
	}
	
	public boolean checkLoc(Location loc, Player ply) {
		if(loc.equals(sign)) {
			Bukkit.dispatchCommand((CommandSender) ply, "ajtntrun join " + arena.getName());
			return true;
		}
		return false;
	}
	
	public String getLine(int line) {
		String raw = msgs.get("signs.lines."+line);
		String s = arena.getPlayerCount() == 1 ? "" : "s";
		String parsed = raw
				.replaceAll("\\{PLAYERS\\}", arena.getPlayerCount()+"")
				.replaceAll("\\{STATE\\}", msgs.get("signs.states."+arena.getState().toString()))
				.replaceAll("\\{NAME\\}", arena.getName())
				.replaceAll("\\{s\\}", s)
				.replaceAll("\\{MAX\\}", arena.getMaxPlayers()+"");
		return parsed;
	}
}
