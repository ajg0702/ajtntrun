package us.ajg0702.tntrun.signs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.Arena.Arena;

public class SignManager {
	
	static SignManager INSTANCE;
	public static SignManager getInstance() {
		return INSTANCE;
	}
	public static SignManager getInstance(Main pl) {
		if(INSTANCE == null) {
			INSTANCE = new SignManager(pl);
		}
		return INSTANCE;
	}
	
	
	List<ArenaSign> signs = new ArrayList<ArenaSign>();
	
	Main pl;
	public SignManager(Main pl) {
		this.pl = pl;
		
		Bukkit.getScheduler().runTaskTimer(pl, new Runnable() {
			public void run() {
				boolean save = false;
				for(Iterator<ArenaSign> i = signs.iterator(); i.hasNext();) {
					ArenaSign sign = i.next();
					if(!sign.update()) {
						i.remove();
						save = true;
					}
				}
				if(save) {
					saveSigns();
				}
			}
		}, 10*20, 20);
	}
	
	
	public void reloadSigns() {
		Bukkit.getScheduler().runTaskLaterAsynchronously(pl, new Runnable() {
			public void run() {
				if(pl.arenafile.isSet("Signs")) {
					@SuppressWarnings("unchecked")
					List<Map<String, Object>> sgns = (List<Map<String, Object>>) pl.arenafile.getList("Signs");
					boolean save = false;
					for(Map<String, Object> sgn : sgns) {
						Arena ar = null;
						boolean found = false;
						for(Arena arena : pl.arenas) {
							if(arena.getName().equals((String)sgn.get("arena"))) {
								ar = arena;
								found = true;
								break;
							}
						}
						if(!found) {
							Location loc = (Location)sgn.get("location");
							int x = loc.getBlockX();
							int y = loc.getBlockY();
							int z = loc.getBlockZ();
							Bukkit.getLogger().warning("[ajTNTRun] Could not find arena '" + sgn.get("arena") + "' for sign at "+x+", "+y+", "+z+"! Deleting the sign.");
							if(loc.getBlock().getState() == null) {
								Bukkit.getLogger().warning("[ajTNTRun] The sign tile is null for an unknown reason. Canceling deletion.");
								continue;
							}
							if(loc.getBlock().getState() instanceof Sign) {
								Sign sign = (Sign) loc.getBlock().getState();
								sign.setLine(0, "");
								sign.setLine(1, "");
								sign.setLine(2, "");
								sign.setLine(3, "");
								sign.update();
							}
							save = true;
							continue;
						}
						
						signs.add(new ArenaSign(ar, (Location)sgn.get("location")));
					}
					if(save) {
						saveSigns();
					}
				} else {
					pl.arenafile.set("Signs", new ArrayList<ArenaSign>());
					try {
						pl.arenafile.save(pl.arenaFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, 20);
	}
	
	
	public void addSign(Arena arena, Location loc) {
		signs.add(new ArenaSign(arena, loc));
		saveSigns();
	}
	
	
	public List<ArenaSign> getSigns() {
		return signs;
	}
	
	
	public void saveSigns() {
		List<Map<String, Object>> sgn = new ArrayList<Map<String, Object>>();
		for(ArenaSign sign : signs) {
			Map<String, Object> signmap = new HashMap<String, Object>();
			signmap.put("arena", sign.arena.getName());
			signmap.put("location", sign.sign);
			sgn.add(signmap);
		}
		pl.arenafile.set("Signs", sgn);
		try {
			pl.arenafile.save(pl.arenaFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removeSign(ArenaSign sign) {
		signs.remove(sign);
		saveSigns();
	}
}
