package us.ajg0702.tntrun;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class RewardCommands {
	static RewardCommands INSTANCE = null;
	public static RewardCommands getInstance(Main plugin) {
		if(INSTANCE == null) {
			INSTANCE = new RewardCommands(plugin);
		}
		return INSTANCE;
	}
	public static RewardCommands getInstance() {
		return INSTANCE;
	}
	
	YamlConfiguration yml;
	File file;
	
	Main plugin;
	private RewardCommands(Main plugin) {
		this.plugin = plugin;
		
		file = new File(plugin.getDataFolder(), "commands.yml");
		yml = YamlConfiguration.loadConfiguration(file);
		
		boolean save = false;
		
		yml.options().header("These are commands that will be executed when a certain part of the game happens.\n If you add \"[p]\" (without quotes) to the beginning of a command, it will be executed as the player instead of the console.");
		if(!yml.isSet("end")) {
			yml.set("end.enabled", false);
			yml.set("end.commands", Arrays.asList("say {PLAYER} won!"));
			save = true;
		}
		if(!yml.isSet("eliminate")) {
			yml.set("eliminate.enabled", false);
			yml.set("eliminate.commands", Arrays.asList("say {PLAYER} was eliminated!"));
			save = true;
		}
		if(save) {
			try {
				yml.save(file);
			} catch (IOException e) {
				plugin.getLogger().warning("Failed to save commands file:");
				e.printStackTrace();
			}
		}
	}
	
	
	public void executeCommands(String event, Player player) {
		if(!yml.isSet(event)) return;
		if(!yml.getBoolean(event+".enabled")) return;
		String pname = (player == null) ? "NOBODY" : player.getName();
		List<String> commands = yml.getStringList(event+".commands");
		for(String command : commands) {
			command = command.replaceAll("\\{PLAYER\\}", pname);
			boolean executeAsPlayer = false;
			if(command.indexOf("[p]") == 0) {
				executeAsPlayer = true;
				command = command.substring(3);
			}
			if(executeAsPlayer) {
				if(player == null) return;
				Bukkit.dispatchCommand(player, command);
			} else {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
			}
		}
	}
}
