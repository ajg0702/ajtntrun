package us.ajg0702.tntrun.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.Messages;
import us.ajg0702.utils.spigot.ConfigFile;

public class ItemManager {
	static ItemManager INSTANCE;
	public static ItemManager getInstance() {
		return INSTANCE;
	}
	public static ItemManager getInstance(Main pl) {
		if(INSTANCE == null) {
			INSTANCE = new ItemManager(pl);
		}
		return INSTANCE;
	}
	
	
	Main pl;
	Messages msgs;
	
	ConfigFile cf;
	
	
	
	private ItemManager(Main pl) {
		this.pl = pl;
		msgs = pl.msgs;
		
		cf = new ConfigFile(pl, "items.yml");
	}
	
	public void reload() {
		cf.reload();
	}
	
	@SuppressWarnings("serial")
	Map<String, String> matConverts = new HashMap<String, String>() {{
        put("BED", "RED_BED");
    }};
	
	public Material validateMat(String matname) {
		Material mat = null;
		try {
			mat = Material.valueOf(matname);
		} catch (IllegalArgumentException e) {}
		
		if(mat == null && matConverts.containsKey(matname)) {
			mat = Material.valueOf(matConverts.get(matname));
		}
		if(mat == null) {
			pl.getLogger().info("[items] The item " + matname + " could not be found in your server version!");
		}
		
		return mat;
	}
	
	
	public ItemStack getItem(String name) {
		Material mat = validateMat(cf.getString(name+"-mat"));
		if(mat == null || mat.equals(Material.AIR) || !isEnabled(name)) {
			return new ItemStack(Material.AIR);
		}
		String itemName = msgs.color("&r"+cf.getString(name+"-name"));
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(itemName);
		im.setLore(colorList(cf.getStringList(name+"-lore")));
		item.setItemMeta(im);
		return item;
	}
	public int getSlot(String name) {
		if(!isEnabled(name)) return 10;
		return cf.getInt(name+"-slot")-1;
	}
	
	public boolean isEnabled(String name) {
		return cf.getBoolean(name+"-enabled");
	}
	
	
	private List<String> colorList(List<String> in) {
		List<String> out = new ArrayList<>();
		for(String s : in) {
			out.add(msgs.color("&r"+s));
		}
		return out;
	}
}
