package us.ajg0702.tntrun;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class Messages {
	
	File file;
	YamlConfiguration msgs;
	
	public String get(String key) {
		String raw;
		if(msgs.isSet(key)) {
			raw = msgs.getString(key);
		} else {
			raw = "&4| &cCould not find the message '" + key + "'!";
		}
		raw = ChatColor.translateAlternateColorCodes('&', raw);
		return raw;
	}
	
	public void reload() {
		msgs = YamlConfiguration.loadConfiguration(file);
	}
	
	static Messages INSTANCE;
	public static Messages getInstance() {
		return INSTANCE;
	}
	
	
	public Messages(JavaPlugin plugin) {
		file = new File(plugin.getDataFolder(), "messages.yml");
		INSTANCE = this;
		msgs = YamlConfiguration.loadConfiguration(file);
		Map<String, String> msgDefaults = new HashMap<String, String>();
		msgDefaults.put("arenaeditor.saved.success", "&aArena saved!");
		msgDefaults.put("arenaeditor.saved.fail", "&cAn error occured while trying to save the arena. &9Check the console for more details.");
		msgDefaults.put("arenaeditor.not-enough-positions", "&cAll positions have not been set!\n&9Make sure you have set 'pos1', 'pos2', and 'startloc'. The 'lobby' and 'maxplayers' settings are optional.");
		msgDefaults.put("arenaeditor.creating.must-pick-a-name", "&cYou must name your arena! &9The arena name must not have any spaces.\n&aIf you have not already, make sure to read the setup guide: https://gitlab.com/ajg0702/ajtntrun/wikis/creating-an-arena");
		msgDefaults.put("arenaeditor.must-put-a-name", "&cPlease enter a name of an arena as the next argument.");
		msgDefaults.put("arenaeditor.creating.success", "&aArena {ARENA} successfully created! Now add positions with /{CMD} arena {ARENA}");
		msgDefaults.put("arenaeditor.create.already-exists", "&cThat arena already exists!");
		
		msgDefaults.put("joining.already-in", "&cYou are already in an arena!");
		msgDefaults.put("joining.no-arena", "&cThat arena could not be found.&9 Did you spell it correctly?");
		msgDefaults.put("leaving.not-in", "&cYou are not in a game!");
		msgDefaults.put("joining.arena-full", "&cThat arena is full!");
		msgDefaults.put("mainlobby.success", "&aSuccessfully set the main lobby!");
		msgDefaults.put("mainlobby.fail", "&cAn error occured while trying to set the main lobby. Check the console for more info.");
		msgDefaults.put("noperm", "&cYou do not have permission to perform this action!");
		
		msgDefaults.put("arenaeditor.set.pos1", "&aFirst position set!");
		msgDefaults.put("arenaeditor.set.pos2", "&aSecond position set!");
		msgDefaults.put("arenaeditor.set.startloc", "&aStarting position set!");
		msgDefaults.put("arenaeditor.set.lobby", "&aLobby position set!");
		msgDefaults.put("arenaeditor.set.maxplayers", "&aMax player count set!");
		
		msgDefaults.put("arenaeditor.more-args.maxplayers", "&cPlease give a number!");
		
		msgDefaults.put("must-be-ingame", "&cYou must be in-game to do this!");
		msgDefaults.put("arenaeditor.cant-find-in-queue", "&cCould not find that arena in the editing queue!\n&aTry creating an arena with &2/{CMD} createarena&a, or load an existing arena with &2/{CMD} edit");
		msgDefaults.put("arena.join",  "&a+ &7{PLAYER} joined the arena! &7(&e{PLAYERS}&7/&e{MAX}&7)");
		msgDefaults.put("arena.leave", "&c- &7{PLAYER} left the arena! &7(&e{PLAYERS}&7/&e{MAX}&7)");
		msgDefaults.put("arena.still-resetting", "&cThat arena is currently resetting. Try again later. ({PERCENT}% done)");
		msgDefaults.put("arena.game-ended", "&aGame ended!");
		msgDefaults.put("arena.teleported-to-lobby", "&aThe game has ended!&9 You have been teleported to the lobby.");
		msgDefaults.put("arena.ingame.spread-out", "&cSpread out!&a The pressure plates will activate in 10 seconds!");
		msgDefaults.put("arena.lobby.bossbar", "&aStarting in &2{TIME} &aseconds");
		msgDefaults.put("arena.lobby.countdown", "&aThe game is starting in {TIME} seconds!");
		msgDefaults.put("arena.lobby.need-more-players", "&9We need {AMOUNT} more player{s} to start the game!");
		msgDefaults.put("arena.pregame.platecountdown", "&cPressure plates activating in {TIME} seconds!");
		msgDefaults.put("arena.end.winner", "&7&m               \n\n&r&a{WINNER} &2wins!\n\n&7&m               ");
		msgDefaults.put("arena.end.nobody", "&7Nobody");
		msgDefaults.put("arena.ingame.air.warn", "&cCareful! &aIf you are over air for much longer, you may be &celiminated!");
		msgDefaults.put("arena.ingame.air.eliminate", "&cYou were eliminated for being on top of air for too long!");
		msgDefaults.put("leaving.left", "&aYou left the game!");
		msgDefaults.put("arenaeditor.import.could-not-find", "&cCould not find an arena with that name. Make sure the caps match too!");
		msgDefaults.put("arenaeditor.import.not-enough-args", "&cEnter an arena name!");
		msgDefaults.put("arenaeditor.import.success", "&aPut arena {ARENA} into the edit queue. &9To edit it, use /{CMD} arena {ARENA}");
		msgDefaults.put("reloaded", "&aReloaded the config and messages!");
		msgDefaults.put("arena.lobby.canceled", "&cThe countdown was canceled because there is not enough players.");
		msgDefaults.put("arena.ingame.eliminate", "&c{PLAYER} eliminated!");
		msgDefaults.put("arena.resetting-status-actionbar", "&cThe arena is currently resetting. {PERCENT}% done.");
		
		
		msgDefaults.put("signs.lines.1", "&e&laj&c&lTNT&b&lRun");
		msgDefaults.put("signs.lines.2", "&b{NAME}");
		msgDefaults.put("signs.lines.3", "{STATE}");
		msgDefaults.put("signs.lines.4", "&9{PLAYERS}/{MAX} player{s}");
		
		msgDefaults.put("signs.states.INITIALIZING", "&cInitializing");
		msgDefaults.put("signs.states.WAITING", "&aWaiting");
		msgDefaults.put("signs.states.STARTING", "&aStarting");
		msgDefaults.put("signs.states.PREGAME", "&ePre-Game");
		msgDefaults.put("signs.states.INGAME", "&eIn-Game");
		msgDefaults.put("signs.states.ENDING", "&eEnding");
		msgDefaults.put("signs.states.RESETTING", "&cResetting");
		
		msgDefaults.put("signs.add.not-a-sign", "&cThe block you are looking at is not a sign!");
		msgDefaults.put("signs.add.could-not-find", "&cCould not find an arena with the name {ARENA}.");
		msgDefaults.put("signs.add.need-an-arena", "&cYou need to specify an arena!");
		msgDefaults.put("signs.add.already-added", "&cThat sign is already an arena sign!");
		msgDefaults.put("signs.add.success", "&aSuccessfully added sign for arena {ARENA}!");
		
		msgDefaults.put("signs.list.none", "&7None");
		
		msgDefaults.put("signs.remove.success", "&aSuccessfully removed the sign you are looking at!");
		msgDefaults.put("signs.remove.not-arenasign", "&cThe sign you are looking at is not an arena sign!");
		msgDefaults.put("signs.remove.not-a-sign", "&cThe block you are looking at is not a sign!");
		
		
		msgDefaults.put("arenaeditor.delete.no-arena", "&cPlease enter an arena to remove");
		msgDefaults.put("arenaeditor.delete.arena-not-found", "&cCould not find an arena with that name. Make sure the capitals match!");
		msgDefaults.put("arenaeditor.delete.success", "&aSuccessfully deleted arena {ARENA}!");
		
		msgDefaults.put("command-blocked", "&cYou cannot execute this command while in a game!");
		
		msgDefaults.put("joining.lobby-not-set", "&cThe main lobby has not been set yet! &9Set it using /tntrun setlobby");
		msgDefaults.put("joining.no-args", "&cYou must give an arena name to join!");
		
		msgDefaults.put("forcestart.success", "&aStarted the arena {ARENA}");
		msgDefaults.put("forcestart.fail.not-waiting", "&cThat game is not in a state that is able to be skipped!");
		msgDefaults.put("forcestart.fail.noarena", "&cPlease enter an arena name to start.");
		msgDefaults.put("forcestart.fail.arena-not-found", "&cCould not find the arena {ARENA}");
		
		msgDefaults.put("editing.none", "&7Not currently editing any arenas.");
		
		msgDefaults.put("list.none", "&7No arenas are set up yet! &9Try &b/tntrun createarena");
		
		msgDefaults.put("stats.header.own", "&7&m       &r &9Your stats &7&m       &r");
		msgDefaults.put("stats.header.other", "&7&m       &r &9{NAME}'s stats &7&m       &r");
		msgDefaults.put("stats.wins", "&b{NUM} &9wins");
		msgDefaults.put("stats.losses", "&b{NUM} &9losses");
		msgDefaults.put("stats.played", "&b{NUM} &9games played");
		msgDefaults.put("stats.cannot-find-player", "&cCannot find that player!");
		
		//msgDefaults.put("", "");
		
		for(String key : msgDefaults.keySet()) {
			if(!msgs.isSet(key)) {
				msgs.set(key, msgDefaults.get(key));
			}
		}
		
		Map<String, String> mv = new HashMap<String, String>();
		//mv.put("before.path", "after.path");
		
		for(String key : mv.keySet()) {
			if(msgs.isSet(key)) {
				msgs.set(mv.get(key), msgs.getString(key));
				msgs.set(key, null);
			}
		}
		
		
		
		msgs.options().header("\n\nThis is the messsages file.\nYou can change any messages that are in this file\n\nIf you want to reset a message back to the default,\ndelete the entire line the message is on and restart the server.\n\t\n\t");
		try {
			msgs.save(file);
		} catch (IOException e) {
			Bukkit.getLogger().warning("[ajAntiXray] Could not save messages file!");
		}
	}
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
}
