package us.ajg0702.tntrun.Arena;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.Messages;
import us.ajg0702.tntrun.RewardCommands;
import us.ajg0702.tntrun.Arena.ArenaState;
import us.ajg0702.tntrun.Stats.StatType;
import us.ajg0702.tntrun.Stats.Stats;
import us.ajg0702.tntrun.items.ItemManager;
import us.ajg0702.tntrun.utils.Debugger;
import us.ajg0702.tntrun.utils.InvManager;
import us.ajg0702.tntrun.utils.VersionSupport;
import us.ajg0702.utils.spigot.Config;
import us.ajg0702.tntrun.Arena.ArenaPlayer;

public class Arena {
	ConfigurationSection rawData;
	int minx;
	int miny;
	int minz;
	int maxx;
	int maxy;
	int maxz;
	
	int maxplayers;
	
	Messages msgs = Main.getPlugin(Main.class).msgs;
	Config config = Main.getPlugin(Main.class).config;
	Stats stats = Main.getPlugin(Main.class).stats;
	Main pl = Main.getPlugin(Main.class);
	String arenaName;
	
	Location startloc;
	Location lobby;
	Logger logger;
	
	public ArenaState state = ArenaState.INITIALIZING;
	
	List<ArenaPlayer> players = new ArrayList<ArenaPlayer>();
	
	Map<Block, Material> rollbackBlocks = new HashMap<Block, Material>();
	
	
	public Arena(ConfigurationSection data) {
		logger = Bukkit.getPluginManager().getPlugin("ajTNTRun").getLogger();
		init(data);
	}
	
	public void saveRollbackBlocks() {
		if(rollbackBlocks.keySet().size() == 0) {
			return;
		}
		//Main.getPlugin(Main.class).setInArenaFile("Arenas."+getName()+".rollbackblocks", this.rollbackBlocks);
		List<Map<String, Object>> a = new ArrayList<Map<String, Object>>();
		for(Block block : rollbackBlocks.keySet()) {
			Map<String, Object> d = new HashMap<String, Object>();
			d.put("x", block.getX());
			d.put("y", block.getY());
			d.put("z", block.getZ());
			d.put("world", block.getWorld().getName());
			d.put("material", ((Material)rollbackBlocks.get(block)).toString());
			a.add(d);
		}
		Main.getPlugin(Main.class).setInArenaFile("Arenas."+getName()+".rollbackblocks", a);
	}
	
	public void walk(Location plate) {
		Block pressureplate = plate.getBlock();
		Material pptype = pressureplate.getType();
		Block sand = plate.clone().add(0, -1, 0).getBlock();
		Material sandtype = sand.getType();
		Block tnt = plate.clone().add(0, -2, 0).getBlock();
		Material tnttype = tnt.getType();
		if(sandtype.toString().contains("AIR") || !tnttype.toString().contains("TNT")) {
			return;
		}
		addRollbackBlock(pressureplate,pptype);
		addRollbackBlock(sand, sandtype);
		addRollbackBlock(tnt, tnttype);
		Bukkit.getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("ajTNTRun"), new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				FallingBlock fallingBlock;
				Location loc = sand.getLocation();
				loc.setX(loc.getX()+0.5);
				loc.setZ(loc.getZ()+0.5);
				BlockData preblockdata = null;
				int preblockolddata = 0;
				Material sandType = null;
				if(VersionSupport.getMinorVersion() > 12) {
					preblockdata = sand.getBlockData();
				} else {
					sandType = sand.getType();
					preblockolddata = sand.getData();
				}
				pressureplate.setType(Material.AIR);
				sand.setType(Material.AIR);
				tnt.setType(Material.AIR);
				if(VersionSupport.getMinorVersion() > 12) {
					fallingBlock = sand.getWorld().spawnFallingBlock(loc, preblockdata);
				} else {
					fallingBlock = sand.getWorld().spawnFallingBlock(loc, sandType, (byte) preblockolddata);
				}
				Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
					public void run() {
						fallingBlock.remove();
					}
				}, 10);
			}
		}, config.getInt("block-fall-delay")); // 10
	}
	
	private void init() {
		init(rawData);
	}
	@SuppressWarnings("unchecked")
	private void init(ConfigurationSection data) {
		String[] arenaPath = data.getCurrentPath().split("\\.");
		arenaName = arenaPath[arenaPath.length-1];
		if(data.isSet("rollbackblocks")) {
			Bukkit.getLogger().warning("[ajTNTRun] The arena '"+arenaName+"' did not get to reset last time, resetting it now.");
			List<Map<String, Object>> a = (List<Map<String, Object>>) data.get("rollbackblocks");
			for(Map<String, Object> d : a) {
				World world = Bukkit.getWorld((String) d.get("world"));
				if(world == null) {
					Bukkit.getLogger().warning("[ajTNTRun] Unable to find world '" + d.get("world")+"'!");
				} else {
					Block block = world.getBlockAt((int)d.get("x"), (int)d.get("y"), (int)d.get("z"));
					Material material = Material.valueOf((String)d.get("material"));
					rollbackBlocks.put(block, material);
				}
			}
			rollbackBlocks();
			data.set("rollbackblocks", null);
			Main.getPlugin(Main.class).setInArenaFile("Arenas."+getName()+".rollbackblocks", null);
		}
		rawData = data;
		state = ArenaState.INITIALIZING;
		
		Debugger.debug("["+getName()+"] Initializing");
		
		Location pos1 = (Location) data.get("pos1");
		Location pos2 = (Location) data.get("pos2");
		
		maxplayers = data.getInt("maxplayers", 16);
		
		
		startloc = (Location) data.get("startloc");
		if(data.isSet("lobby")) {
			lobby = (Location) data.get("lobby");
		} else {
			lobby = startloc;
		}
		
		rollbackBlocks = new HashMap<Block, Material>();
		
		maxx = (pos1.getBlockX() < pos2.getBlockX()) ? pos2.getBlockX() : pos1.getBlockX();
		minx = (pos1.getBlockX() > pos2.getBlockX()) ? pos2.getBlockX() : pos1.getBlockX();
		
		maxy = (pos1.getBlockY() < pos2.getBlockY()) ? pos2.getBlockY() : pos1.getBlockY();
		miny = (pos1.getBlockY() > pos2.getBlockY()) ? pos2.getBlockY() : pos1.getBlockY();
		
		maxz = (pos1.getBlockZ() < pos2.getBlockZ()) ? pos2.getBlockZ() : pos1.getBlockZ();
		minz = (pos1.getBlockZ() > pos2.getBlockZ()) ? pos2.getBlockZ() : pos1.getBlockZ();
		
		Debugger.debug("["+getName()+"] [init] changeState WAITING");
		changeState(ArenaState.WAITING);
		state = ArenaState.WAITING;
	}
	
	public List<ArenaPlayer> getPlayers() {
		return this.players;
	}
	
	public int getPlayerCount() {
		return this.players.size();
	}
	
	public int getMaxPlayers() {
		return maxplayers;
	}
	
	public ArenaState getState() {
		//Bukkit.getLogger().info(state.toString());
		return state;
	}
	
	public ArenaPlayer addPlayer(Player player) {
		if(!rawData.getParent().getParent().isSet("mainlobby")) {
			player.sendMessage(msgs.get("joining.lobby-not-set"));
			return null;
		}
		if(getPlayerCount() >= getMaxPlayers() && !player.hasPermission("atr.joinfull")) {
			player.sendMessage(msgs.get("joining.arena-full"));
			return null;
		}
		
		boolean allowJoinResetting = !state.equals(ArenaState.RESETTING);
		if(state.equals(ArenaState.RESETTING) && config.getBoolean("allow-join-while-resetting")) {
			if(startloc.equals(lobby)) {
				allowJoinResetting = false;
			} else {
				allowJoinResetting = true;
			}
		} else if(state.equals(ArenaState.RESETTING)) {
			allowJoinResetting = false;
		}
		
		if(state != ArenaState.INITIALIZING && allowJoinResetting) {
			try {
				InvManager.saveInventory(player);
			} catch (IOException e) {
				logger.severe("Unable to save "+player.getName()+"'s inventory:");
				e.printStackTrace();
				return null;
			}
			ArenaPlayer ply = new ArenaPlayer(player, this);
			players.add(ply);
			if(!(state == ArenaState.WAITING || state == ArenaState.STARTING || state == ArenaState.RESETTING)) {
				player.teleport(startloc);
				if(ply.state.equals(PlayerState.SPECTATING)) {
					player.getPlayer().setVelocity(new Vector().setY(0.75));
				} else {
					player.setFlying(false);
					player.setAllowFlight(false);
				}
			} else {
				player.teleport(lobby);
			}
			//player.sendMessage("Arena is in state " + state.toString());
			for(ArenaPlayer playr : players) {
				playr.getPlayer().sendMessage(
						msgs.get("arena.join")
						.replaceAll("\\{PLAYER\\}", player.getName())
						.replaceAll("\\{PLAYERS\\}", getPlayerCount()+"")
						.replaceAll("\\{MAX\\}", getMaxPlayers()+"")
						);
			}
			
			checkStart();
			for (PotionEffect effect : player.getActivePotionEffects()) {
		        player.removePotionEffect(effect.getType());
			}
			
			player.getInventory().clear();
			
			Bukkit.getScheduler().runTask(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					populateInventory(ply);
				}
			});
			
			
			return ply;
		}
		Double percent = (double)rollbackProgress / (double)rollbackTotal;
		percent = (double) Math.round(percent * 1000)/10;
		player.sendMessage(msgs.get("arena.still-resetting").replaceAll("\\{PERCENT\\}", percent+""));
		return null;
	}
	
	public void checkStart() {
		int minPlayers = ((int) config.get("min-players"));
		if(state.equals(ArenaState.WAITING) && this.getPlayerCount() >= minPlayers) {
			changeState(ArenaState.STARTING);
		} else if(state.equals(ArenaState.WAITING)) {
			for(ArenaPlayer plyr : players) {
				int morePlayers = minPlayers - this.getPlayerCount();
				String s = "s";
				if(morePlayers == 1) s = "";
				plyr.getPlayer().sendMessage(msgs.get("arena.lobby.need-more-players")
						.replaceAll("\\{AMOUNT\\}", morePlayers+"")
						.replaceAll("\\{s\\}", s));
			}
		}
	}
	
	public void addRollbackBlock(Block block, Material type) {
		if(!rollbackBlocks.containsKey(block)) {
			rollbackBlocks.put(block, type);
			//Bukkit.getLogger().info("Added block " + type.toString() + "/" + rollbackBlocks.get(block));
		}
	}
	
	public boolean changeState(ArenaState newstate) {
		Debugger.debug("[changestate] ["+getName()+"] Changing state to "+newstate);
		ArenaState current = state;
		if(newstate != current) {
			
			//Bukkit.getLogger().info("[ajTNTRUN] ["+getName()+"] Changing to " + newstate.toString());
			state = newstate;
			if(newstate != ArenaState.RESETTING) {
				for(ArenaPlayer ply : players) {
					populateInventory(ply);
				}
			}
			switch(newstate) {
			case INITIALIZING:
				if(players.size() != 0 && current != ArenaState.RESETTING) {
					Iterator<ArenaPlayer> it = players.iterator();
					while(it.hasNext()) {
						ArenaPlayer player = it.next();
						kickPlayer(player, "the arena is re-initializing", false);
						it.remove();
					}
				}
				//Bukkit.getLogger().info("[ajTNTRun] Re-initializing arena " + getName());
				init();
				return true;
			case ENDING:
				if(state == ArenaState.RESETTING || state == ArenaState.WAITING) {
					state = current;
					break;
				}
				for(ArenaPlayer player : players) {
					player.getPlayer().sendMessage(msgs.get("arena.game-ended"));
					player.getPlayer().setGameMode(GameMode.SURVIVAL);
					player.getPlayer().setVelocity(new Vector().setY(0.75));
					Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
						public void run() {
							player.getPlayer().setAllowFlight(true);
							player.getPlayer().setFlying(true);
						}
 					}, 10);
				}
				int fireworkTimer = Bukkit.getScheduler().runTaskTimer(Main.getPlugin(Main.class), new Runnable() {
					public void run() {
						Random r = new Random();
						int x = getRandInt(minx, maxx);
						int y = getRandInt(miny, maxy);
						int z = getRandInt(minz, maxz);
						
						World world = startloc.getWorld();
						Location loc = new Location(world, x, y, z);
						if(!loc.getBlock().getType().equals(Material.AIR)) {
							r = new Random();
							x = getRandInt(minx, maxx);
							y = getRandInt(miny, maxy);
							z = getRandInt(minz, maxz);
							loc = new Location(world, x, y, z);
						}
						if(!loc.getBlock().getType().equals(Material.AIR)) {
							return;
						}
						
						Firework fw = (Firework) world.spawnEntity(loc, EntityType.FIREWORK);
						FireworkMeta fwmeta = fw.getFireworkMeta();
						
						int tr = r.nextInt(3) + 1;
						Type type = Type.BALL;
						if(tr == 1) type = Type.BALL_LARGE;
						if(tr == 2) type = Type.STAR;
						if(tr == 3) type = Type.BURST;
						
						Color c1 = getColor(r.nextInt(17)+1);
						Color c2 = getColor(r.nextInt(17)+1);
						
						FireworkEffect effect = FireworkEffect.builder()
								.flicker(r.nextBoolean())
								.withColor(c1)
								.withFade(c2)
								.with(type)
								.trail(r.nextBoolean()).build();
						fwmeta.addEffect(effect);
						fwmeta.setPower(r.nextInt(2)+1);
						
						fw.setFireworkMeta(fwmeta);
					}
				}, 0, 10).getTaskId();
				Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
					public void run() {
						Bukkit.getScheduler().cancelTask(fireworkTimer);
						Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
							public void run() {
								if(state.equals(ArenaState.ENDING)) {
									changeState(ArenaState.RESETTING);
								}
							}
						}, 40);
					}
 				}, (long)(((int)config.get("end-duration")-2)*20));
				return true;
			case INGAME:
				for(ArenaPlayer player : players) {
					if(!player.state.equals(PlayerState.SPECTATING)) {
						player.state = PlayerState.INGAME;
						
						String soundraw = config.getString("game-start-sound");
						String sound = soundraw.split(";")[0];
						float pitch = Float.valueOf(soundraw.split(";")[1]);
						player.getPlayer().playSound(player.getPlayer().getLocation(), sound, 1, pitch);
						
						Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(Main.class), new Runnable() {
							public void run() {
								UUID uuid = player.getPlayer().getUniqueId();
								stats.add(uuid, StatType.PLAYED, 1);
							}
						});
					}
				}
				return true;
			case RESETTING:
				try {
					for(Iterator<ArenaPlayer> iterator = players.iterator(); iterator.hasNext();) {
						ArenaPlayer plyr = iterator.next();
						plyr.getPlayer().sendMessage(msgs.get("arena.teleported-to-lobby"));
						plyr.getPlayer().setFlying(false);
						plyr.getPlayer().setAllowFlight(false);
						kickPlayer(plyr, "none", false);
						iterator.remove();
					}
				} catch(Exception e) {}
				rollbackBlocks();
				
				startResetActionbar();
				
				return true;
			case STARTING:
				startLobbyCountdown();
				return true;
			case PREGAME:
				cancelCdTask();
				state = ArenaState.PREGAME;
				for(ArenaPlayer player : players) {
					player.getPlayer().teleport(startloc);
					player.state = PlayerState.INGAME;
					player.getPlayer().sendMessage(msgs.get("arena.ingame.spread-out"));
				}
				startPregameCountdown();
			case WAITING:
				Bukkit.getScheduler().runTask(pl, new Runnable() {
					public void run() {
						checkStart();
					}
				});
				break;
			}
		} else {
			return true;
		}
		state = current;
		return false;
	}
	
	int resetActionbarTask = -1;
	public void startResetActionbar() {
		if(resetActionbarTask != -1) {
			Bukkit.getScheduler().cancelTask(resetActionbarTask);
			resetActionbarTask = -1;
		}
		resetActionbarTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run() {
				if(state != ArenaState.RESETTING) {
					Bukkit.getScheduler().cancelTask(resetActionbarTask);
					resetActionbarTask = -1;
				}
				
				Double percent = (double)rollbackProgress / (double)rollbackTotal;
				percent = (double) Math.round(percent * 1000)/10;
				for(ArenaPlayer ply : players) {
					Player p = ply.getPlayer();
					VersionSupport.sendActionBar(p, msgs.get("arena.resetting-status-actionbar").replaceAll("\\{PERCENT\\}", percent+""));
				}
			}
		}, 0, 10);
	}
	
	BossBar cdbar = null;
	int cdTask; 
	int lobbycdTask;
	int lobbyCountdown;
	public void startLobbyCountdown() {
		if(cdbar != null || (cdbar != null && !cdbar.isVisible())) {
			if(cdbar != null) {
				cdbar.setVisible(false);
				cdbar.removeAll();
			}
			cdbar = null;
		}
		lobbyCountdown = (int) config.get("lobby-countdown");
		//Bukkit.getLogger().info("Starting lobby countdown!");
		if(VersionSupport.getMinorVersion() > 8) {
			cdbar = Bukkit.createBossBar(msgs.get("arena.lobby.bossbar").replaceAll("\\{TIME\\}", lobbyCountdown+""), BarColor.BLUE, BarStyle.SOLID);
			cdbar.setProgress(1);
			for(ArenaPlayer player : players) {
				cdbar.addPlayer(player.getPlayer());
			}
		}
		state = ArenaState.STARTING;
		int startingCountdown = lobbyCountdown;
		lobbycdTask = Bukkit.getScheduler().runTaskTimer(Main.getProvidingPlugin(Main.class), new Runnable() {
			public void run() {
				if(!state.equals(ArenaState.STARTING)) {
					cancelCdTask();
					return;
				}
				lobbyCountdown--;
				if(lobbyCountdown <= 0) {
					cancelCdTask();
					if(cdbar != null) {
						cdbar.removeAll();
						for(Player player : cdbar.getPlayers()) {
							cdbar.removePlayer(player);
						}
					}
					changeState(ArenaState.PREGAME);
					return;
				} else if(getPlayerCount() < (int)config.get("min-players")) {
					cancelCdTask();
					if(cdbar != null) {
						cdbar.removeAll();
						for(Player player : cdbar.getPlayers()) {
							player.sendMessage(msgs.get("arena.lobby.canceled"));
							cdbar.removePlayer(player);
						}
					}
				} else if(lobbyCountdown <= 5) {
					for(ArenaPlayer player : players) {
						player.getPlayer().sendMessage(msgs.get("arena.lobby.countdown").replaceAll("\\{TIME\\}", lobbyCountdown+""));
					}
				}
				if(cdbar == null) {
					for(ArenaPlayer player : players) {
						Player ply = player.getPlayer();
						VersionSupport.sendActionBar(ply, msgs.get("arena.lobby.bossbar").replaceAll("\\{TIME\\}", lobbyCountdown+""));
					}
				} else {
					double progress = ((double)lobbyCountdown)/((double)startingCountdown);
					for(ArenaPlayer player : players) {
						cdbar.addPlayer(player.getPlayer());
						//player.getPlayer().sendMessage(progress+"");
					}
					cdbar.setTitle(msgs.get("arena.lobby.bossbar").replaceAll("\\{TIME\\}", lobbyCountdown+""));
					cdbar.setProgress(progress);
				}
			}
		}, 0L, 20L).getTaskId();
	}
	private void cancelCdTask() {
		Bukkit.getScheduler().cancelTask(cdTask);
		Bukkit.getScheduler().cancelTask(lobbycdTask);
	}
	public void start() {
		if(getPlayerCount() < (int)config.get("min-players")) {
			changeState(ArenaState.PREGAME);
		} else {
			lobbyCountdown = 0;
		}
	}
	
	int pregameCountdown;
	public void startPregameCountdown() {
		pregameCountdown = (int) config.get("plate-countdown");
		Debugger.debug("["+getName()+"] Starting pregame countdown");
		//cdbar = Bukkit.createBossBar("§aStarting in §2" + pregameCountdown + " §aseconds", BarColor.BLUE, BarStyle.SOLID);
		//cdbar.setProgress(1);
		//int startingCountdown = pregameCountdown;
		state = ArenaState.PREGAME;
		cdTask = Bukkit.getScheduler().runTaskTimer(Main.getProvidingPlugin(Main.class), new Runnable() {
			public void run() {
				if(!state.equals(ArenaState.PREGAME) && !state.equals(ArenaState.STARTING) && !state.equals(ArenaState.WAITING)) {
					Debugger.debug("[pregamecd] Stopping pre-game countdown because arenastate is "+state);
					cancelCdTask();
					return;
				}
				pregameCountdown--;
				if(pregameCountdown <= 0) {
					changeState(ArenaState.INGAME);
					Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
						public void run() {
							for(ArenaPlayer player : players) {
								if(player.getPlayer().getLocation().equals(startloc)) {
									walk(player.getPlayer().getLocation());
									break;
								}
							}
						}
					}, 20);
					cancelCdTask();
					return;
				} else if(pregameCountdown <= 5) {
					for(ArenaPlayer player : players) {
						player.getPlayer().sendMessage(msgs.get("arena.pregame.platecountdown").replaceAll("\\{TIME\\}", pregameCountdown+""));
						
						String soundraw = config.getString("pregame-countdown-sound");
						String sound = soundraw.split(";")[0];
						float pitch = Float.valueOf(soundraw.split(";")[1]);
						player.getPlayer().playSound(player.getPlayer().getLocation(), sound, 1, pitch);
					}
				}
				state = ArenaState.PREGAME;
				//cdbar.setTitle("§aStarting in §2" + pregameCountdown + " §aseconds");
				//cdbar.setProgress(pregameCountdown/startingCountdown);
			}
		}, 0L, 20L).getTaskId();
	}
	
	public String getName() {
		return arenaName;
	}
	
	public void kickPlayer(ArenaPlayer player) {
		kickPlayer(player, "none");
	}
	public void kickPlayer(ArenaPlayer player, String reason) {
		kickPlayer(player, reason, true);
	}
	public void kickPlayer(ArenaPlayer player, String reason, boolean remove) {
		if(players.contains(player)) {
			if(rawData.getParent().getParent().isSet("mainlobby")) {
				player.getPlayer().teleport((Location)rawData.getParent().getParent().get("mainlobby"));
			}
			try {
				player.getPlayer().getInventory().clear();
				InvManager.restoreInventory(player.getPlayer());
			} catch (IOException e) {
				logger.severe("Unable to restore "+player.getName()+"'s inventory:");
				e.printStackTrace();
			}
			if(config.get("enable-bungee") != null) {
				if((boolean) config.get("enable-bungee")) {
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					out.writeUTF("Connect");
					out.writeUTF(config.get("bungee-remove-to").toString());
					player.getPlayer().sendPluginMessage(Main.getPlugin(Main.class), "BungeeCord", out.toByteArray());
				}
			}
			if(!reason.equalsIgnoreCase("none")) {
				player.getPlayer().sendMessage("§cYou were kicked from the game because " + reason);
			}
			if(remove) {
				players.remove(player);
			}
			player.getPlayer().setGameMode(GameMode.SURVIVAL);
			Player ply = player.getPlayer();
			Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					ply.setFlying(false);
					ply.setAllowFlight(false);
					ply.setFlying(false);
				}
			}, 5);
			if(cdbar != null) {
				cdbar.removePlayer(player.getPlayer());
			}
			if(state != ArenaState.RESETTING && state != ArenaState.ENDING) {
				for(ArenaPlayer plyr : players) {
					plyr.getPlayer().sendMessage(
							msgs.get("arena.leave")
							.replaceAll("\\{PLAYER\\}", player.getName())
							.replaceAll("\\{PLAYERS\\}", getPlayerCount()+"")
							.replaceAll("\\{MAX\\}", getMaxPlayers()+"")
							);	
				}
			}
			player = null;
		}
		checkEnd();
	}
	
	public boolean checkEnd() {
		List<ArenaPlayer> playingPlayers = new ArrayList<ArenaPlayer>();
		for(ArenaPlayer ply : players) {
			//ply.getPlayer().sendMessage(state.toString());
			if(ply.getState().equals(PlayerState.INGAME)) {
				playingPlayers.add(ply);
			}
		}
		if(playingPlayers.size() <= 1 && getState() != ArenaState.WAITING && getState() != ArenaState.RESETTING && getState() != ArenaState.STARTING && getState() != ArenaState.RESETTING && getState() != ArenaState.ENDING) {
			Player winner = playingPlayers.size() <= 0 ? null : playingPlayers.get(0).getPlayer();
			if(winner != null) {
				Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(Main.class), new Runnable() {
					public void run() {
						stats.add(winner.getUniqueId(), StatType.WINS, 1);
					}
				});
			}
			for(ArenaPlayer player : players) {
				String winnerName = (winner == null ? msgs.get("arena.end.nobody") : winner.getName());
				player.getPlayer().sendMessage(msgs.get("arena.end.winner").replaceAll("\\{WINNER\\}", winnerName));
			}
			RewardCommands.getInstance().executeCommands("end", winner);
			changeState(ArenaState.ENDING);
			return true;
		} else if(getState() == ArenaState.WAITING || getState() == ArenaState.STARTING) {
			int morePlayers = (int)config.get("min-players") - this.getPlayerCount();
			if(morePlayers >= 1) {
				cancelCdTask();
				for(ArenaPlayer plyr : players) {
					String s = "s";
					if(cdbar != null) {
						cdbar.removePlayer(plyr.getPlayer());
					}
					if(morePlayers == 1) s = "";
					plyr.getPlayer().sendMessage(msgs.get("arena.lobby.need-more-players")
							.replaceAll("\\{AMOUNT\\}", morePlayers+"")
							.replaceAll("\\{s\\}", s));
				}
				state = ArenaState.WAITING;
			}
			return true;
		} else {
			return false;
		}
	}
	
	public void eliminate(ArenaPlayer player) {
		if(state.equals(ArenaState.INGAME)) {
			player.state = PlayerState.SPECTATING;
			player.getPlayer().setGameMode(GameMode.SPECTATOR);
			
			RewardCommands.getInstance().executeCommands("eliminate", player.getPlayer());
			
			List<ArenaPlayer> tempplayers = new ArrayList<ArenaPlayer>();
			tempplayers.addAll(players);
			for(ArenaPlayer ply : tempplayers) {
				ply.getPlayer().sendMessage(msgs.get("arena.ingame.eliminate").replaceAll("\\{PLAYER\\}", player.getName()));
			}
			
			checkEnd();
			
			Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					stats.add(player.getPlayer().getUniqueId(), StatType.LOSSES, 1);
				}
			});
			
			player.getPlayer().teleport(startloc);
			player.getPlayer().setAllowFlight(true);
			player.getPlayer().setFlying(true);
			player.getPlayer().setVelocity(new Vector().setY(0.5));
		}
	}
	int rollbackTotal;
	int rollbackProgress;
	Double rollbackMs;
	public void rollbackBlocks() {
		
		Double rollbackSpeed = 1.0/Main.getPlugin(Main.class).getConfig().getDouble("rollback-speed");
		
		List<ArenaPlayer> tempplayers = new ArrayList<ArenaPlayer>(players);
		for(Iterator<ArenaPlayer> it = tempplayers.iterator(); it.hasNext();) {
			ArenaPlayer player = it.next();
			kickPlayer(player, "none", false);
		}
		tempplayers = null;
		state = ArenaState.RESETTING;
		//Bukkit.broadcastMessage("§9Starting reset of arena " +getName());
		Double i = (double) 1;
		List<Block> tnt = new ArrayList<Block>();
		Map<Block, Material> sand = new HashMap<Block, Material>();
		Map<Block, Material> plates = new HashMap<Block, Material>();
		for(Iterator<Block> iterator = rollbackBlocks.keySet().iterator(); iterator.hasNext();) {
			rollbackTotal = rollbackBlocks.keySet().size();
			rollbackProgress = 0;
			Block block = iterator.next();
			Material type = rollbackBlocks.get(block);
			//Bukkit.getLogger().info(block.getType().toString());
			if(type == Material.TNT) {
				tnt.add(block);
				//Bukkit.getLogger().info("TNT");
			} else if(type == Material.SAND || type == Material.GRAVEL) {
				sand.put(block, type);
				//Bukkit.getLogger().info("SAND");
			} else {
				plates.put(block, type);
				//Bukkit.getLogger().info("PLATE");
			}
			state = ArenaState.RESETTING;
		}
		for(Block block : tnt) {
			Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					block.setType(Material.TNT);
					state = ArenaState.RESETTING;
					rollbackProgress++;
					//Bukkit.getLogger().info("+TNT");
				}
			}, (long)Math.floor(i));
			i += rollbackSpeed;
		}
		for(Block block : sand.keySet()) {
			Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					block.setType(sand.get(block));
					state = ArenaState.RESETTING;
					rollbackProgress++;
					//Bukkit.getLogger().info("+SAND");
				}
			}, (long)Math.floor(i));
			i += rollbackSpeed;
		}
		for(Block block : plates.keySet()) {
			Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
				public void run() {
					block.setType(plates.get(block));
					state = ArenaState.RESETTING;
					rollbackProgress++;
					//Bukkit.getLogger().info("+PLATE - " + plates.get(block).toString());
				}
			}, (long)Math.floor(i));
			i += rollbackSpeed;
		}
		Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new Runnable() {
			public void run() {
				Debugger.debug("[reset] Reset should be done now. Changing arena state..");
				changeState(ArenaState.INITIALIZING);
				rollbackBlocks = new HashMap<Block, Material>();
				//Bukkit.broadcastMessage("§aShould be done resetting arena " + getName() + " (" + rollbackMs + "ms)");
			}
		}, (long)Math.floor(i)+5);
		Debugger.debug("[reset] Reset should be done after "+(long)Math.floor(i)+5+" ticks");
		rollbackMs = ((i*20)/1000);
	}
	
	public void checkPlayerInBounds(Player player) {
		ArenaPlayer ply = findPlayer(player);
		if(ply != null) {
			//player.sendMessage("Arena is " + state.toString());
			Location loc = player.getLocation();
			int x = loc.getBlockX();
			int y = loc.getBlockY();
			int z = loc.getBlockZ();
			boolean xbounds = x < maxx && x > minx;
			boolean ybounds = y < maxy && y > miny;
			boolean zbounds = z < maxz && z > minz;
			//player.sendMessage(" " + miny + "/"+ y + "/"+maxy);
			if((!xbounds || !ybounds || !zbounds)) {
				//player.sendMessage(state.toString());
				if(state.equals(ArenaState.STARTING) || state.equals(ArenaState.WAITING) || state.equals(ArenaState.RESETTING)) {
					//Bukkit.getLogger().info("Arena is pregame/starting, teleporting player");
					if(checkInBounds(lobby)) {
						player.teleport(lobby);
					}
					return;
				}
				if(state.equals(ArenaState.ENDING) || state.equals(ArenaState.PREGAME)) {
					player.teleport(startloc);
					return;
				}
				if(ply.state.equals(PlayerState.INGAME)) {
					eliminate(ply);
					return;
				} else {
					player.teleport(startloc);
					player.getPlayer().setVelocity(new Vector().setY(0.75));
					return;
				}
			}
		}
	}
	
	public void clickItem(ArenaPlayer player, int slot) {
		switch(state) {
		case ENDING:
			break;
		case INGAME:
			break;
		case INITIALIZING:
			break;
		case PREGAME:
			break;
		case RESETTING:
		case STARTING:
		case WAITING:
			
			if(slot == 0) {
				Bukkit.dispatchCommand(player.getPlayer(), "ajtntrun stats");
			} else if(slot == 8) {
				Bukkit.dispatchCommand(player.getPlayer(), "ajtntrun leave");
			}
			
			break;
		default:
			break;
		
		}
	}
	public void populateInventory(ArenaPlayer player) {
		Debugger.debug("Populating inventory items for " +player.getName()+ " with state "+state);
		Player ply = player.getPlayer();
		PlayerInventory inv = ply.getInventory();
		ItemManager im = ItemManager.getInstance();
		inv.clear();
		switch(state) {
		case ENDING:
			break;
		case INGAME:
			break;
		case INITIALIZING:
			break;
		case PREGAME:
			break;
		case RESETTING:
		case STARTING:
		case WAITING:
			
			inv.setItem(im.getSlot("lobby-leave"), im.getItem("lobby-leave"));
			inv.setItem(im.getSlot("lobby-stats"), im.getItem("lobby-stats"));
			
			break;
		default:
			break;
		
		}
	}
	
	
	
	public boolean checkInBounds(Location loc) {
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		boolean xbounds = x < maxx && x > minx;
		boolean ybounds = y < maxy && y > miny;
		boolean zbounds = z < maxz && z > minz;
		
		return xbounds && ybounds && zbounds;
	}
	
	public void checkAirPlayers() {
		if(state != ArenaState.INGAME) return;
		for(ArenaPlayer player : players) {
			if(!player.getState().equals(PlayerState.SPECTATING)) {
				player.checkAir();
			}
			player.getPlayer().setSaturation(20);
			player.getPlayer().setFoodLevel(20);
		}
	}
	
	public ArenaPlayer findPlayer(Player ply) {
		for(ArenaPlayer player : players) {
			if(player.getPlayer().equals(ply)) {
				return player;
			}
		}
		return null;
	}
	
	
	//from https://www.mkyong.com/java/java-generate-random-integers-in-a-range/  thanks!
	private static int getRandInt(int min, int max) {
		if (min > max) {
			Random r = new Random();
			return r.nextInt((min - max) + 1) + max;
		} else {
			Random r = new Random();
			return r.nextInt((max - min) + 1) + min;
		}
	}
	
	private Color getColor(int i) {
		Color c = null;
		if(i==1){
			c=Color.AQUA;
		}
		if(i==2){
			c=Color.BLACK;
		}
		if(i==3){
			c=Color.BLUE;
		}
		if(i==4){
			c=Color.FUCHSIA;
		}
		if(i==5){
			c=Color.GRAY;
		}
		if(i==6){
			c=Color.GREEN;
		}
		if(i==7){
			c=Color.LIME;
		}
		if(i==8){
			c=Color.MAROON;
		}
		if(i==9){
			c=Color.NAVY;
		}
		if(i==10){
			c=Color.OLIVE;
		}
		if(i==11){
			c=Color.ORANGE;
		}
		if(i==12){
			c=Color.PURPLE;
		}
		if(i==13){
			c=Color.RED;
		}
		if(i==14){
			c=Color.SILVER;
		}
		if(i==15){
			c=Color.TEAL;
		}
		if(i==16){
			c=Color.WHITE;
		}
		if(i==17){
			c=Color.YELLOW;
		}
		 
		return c;
		}
}
