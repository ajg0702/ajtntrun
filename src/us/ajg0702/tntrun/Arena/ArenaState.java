package us.ajg0702.tntrun.Arena;

public enum ArenaState {
	INITIALIZING, WAITING, STARTING, PREGAME, INGAME, ENDING, RESETTING
}
