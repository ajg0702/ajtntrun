package us.ajg0702.tntrun.Arena;

public enum PlayerState {
	WAITING, INGAME, SPECTATING, LEAVING
}
