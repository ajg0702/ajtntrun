package us.ajg0702.tntrun.Arena;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.Messages;
import us.ajg0702.utils.spigot.VersionSupport;

public class ArenaPlayer {
	Arena arena;
	Player player;
	PlayerState state;
	Messages msgs = Main.getPlugin(Main.class).msgs;
	public ArenaPlayer(Player ply, Arena parent) {
		arena = parent;
		player = ply;
		//ply.sendMessage(arena.getState().toString());
		
		joined = System.currentTimeMillis();
		
		player.setSaturation(20);
		player.setFoodLevel(20);
		
		switch(arena.getState()) {
		case WAITING:
			state = PlayerState.WAITING;
			ply.setGameMode(GameMode.SURVIVAL);
			break;
		case STARTING:
			state = PlayerState.WAITING;
			ply.setGameMode(GameMode.SURVIVAL);
			break;
		case INGAME:
			state = PlayerState.SPECTATING;
			ply.setGameMode(GameMode.SPECTATOR);
			break;
		case ENDING:
			state = PlayerState.SPECTATING;
			ply.setGameMode(GameMode.SPECTATOR);
			break;
		case INITIALIZING:
			state = PlayerState.WAITING;
			ply.setGameMode(GameMode.SURVIVAL);
			break;
		case PREGAME:
			state = PlayerState.SPECTATING;
			ply.setGameMode(GameMode.SPECTATOR);
			break;
		case RESETTING:
			state = PlayerState.WAITING;
			ply.setGameMode(GameMode.SURVIVAL);
			break;
		default:
			state = PlayerState.SPECTATING;
			ply.setGameMode(GameMode.SPECTATOR);
			break;

		}
		
	}
	
	
	long joined;
	public long getJoined() {
		return joined;
	}
	
	Map<Player, Integer> airPlayers = new HashMap<Player, Integer>();
	public void checkAir() {
		int elimTime = arena.pl.config.getInt("air-eliminate-time");
		if(elimTime < 0) return;
		
		int x = player.getLocation().getBlockX();
		int y = player.getLocation().getBlockY();
		int z = player.getLocation().getBlockZ();
		
		if(player.getWorld().getBlockAt(x, y-1, z).getType().toString().contains("AIR") &&
		   player.getWorld().getBlockAt(x, y-2, z).getType().toString().contains("AIR")) {
			//player.sendMessage(airPlayers.get(player)+"");
			if(airPlayers.containsKey(player)) {
				int before = airPlayers.get(player);
				int after = before + 1;
				airPlayers.put(player, after);
				//VersionSupport.sendActionBar(player, ""+((((double)after)/((double)elimTime))*100));
				if(after == Math.floor(elimTime/2)) {
					player.sendMessage(msgs.get("arena.ingame.air.warn"));
				} else if(after == elimTime) {
					player.sendMessage(msgs.get("arena.ingame.air.eliminate"));
					arena.eliminate(this);
				}
			} else {
				airPlayers.put(player, 0);
				//VersionSupport.sendActionBar(player, ""+0);
			}
		} else if(airPlayers.containsKey(player)) {
			//player.sendMessage(airPlayers.get(player)+"");
			int before = airPlayers.get(player);
			int after = before - 2;
			if(after < 0) {
				after = 0;
			}
			airPlayers.put(player, after);
			//VersionSupport.sendActionBar(player, ""+((((double)after)/((double)elimTime))*100));
		}
		
	}
	
	public void leave() {
		state = PlayerState.LEAVING;
		arena.kickPlayer(this, "none");
		player.sendMessage(msgs.get("leaving.left"));	
	}
	
	public String getName() {
		return player.getName();
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Arena getArena() {
		return arena;
	}
	
	public PlayerState getState() {
		return state;
	}
}
