package us.ajg0702.tntrun;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import us.ajg0702.tntrun.Arena.Arena;
import us.ajg0702.tntrun.Arena.ArenaPlayer;
import us.ajg0702.tntrun.Arena.ArenaState;
import us.ajg0702.tntrun.Stats.StatType;
import us.ajg0702.tntrun.Stats.Stats;
import us.ajg0702.tntrun.items.ItemManager;
import us.ajg0702.tntrun.signs.ArenaSign;
import us.ajg0702.tntrun.signs.SignManager;
import us.ajg0702.tntrun.utils.Updater;

public class Commands implements CommandExecutor, TabCompleter {
	
	Main pl;
	Messages msgs;
	Stats stats;
	SignManager signs;
	public Commands(Main pl) {
		this.pl = pl;
		msgs = pl.msgs;
		stats = pl.stats;
		signs = SignManager.getInstance();
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		@SuppressWarnings("unused")
		Player sply = null;
		if(sender instanceof Player) {
			sply = (Player) sender;
		}
		if(args.length > 0) {
			if(args[0].equalsIgnoreCase("arena")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length >= 2) {
					Boolean found = false;
					int i = -1;
					String arenaName = args[1];
					Map<String, Object> arena = new HashMap<String, Object>();
					for(Map<String, Object> ar : pl.inProgressArenas) {
						if(ar.get("name").toString().equals(arenaName)) {
							i++;
							found = true;
							arena = ar;
							break;
						}
					}

					if(!found) {
						sender.sendMessage(msgs.get("arenaeditor.cant-find-in-queue").replaceAll("\\{CMD\\}", label));
						return true;
					}
					if(args.length >= 3) {
						if(args[2].equalsIgnoreCase("startloc")) {
							if(!(sender instanceof Player)) {
								sender.sendMessage(msgs.get("must-be-ingame"));
								return true;
							}
							arena.put("startloc", ((Player)sender).getLocation());
							pl.inProgressArenas.set(i, arena);
							sender.sendMessage(msgs.get("arenaeditor.set.startloc"));
						} else if(args[2].equalsIgnoreCase("lobby")) {
							if(!(sender instanceof Player)) {
								sender.sendMessage(msgs.get("must-be-ingame"));
								return true;
							}
							arena.put("lobby", ((Player)sender).getLocation());
							pl.inProgressArenas.set(i, arena);
							sender.sendMessage(msgs.get("arenaeditor.set.lobby"));
						} else if(args[2].equalsIgnoreCase("pos1")) {
							if(!(sender instanceof Player)) {
								sender.sendMessage(msgs.get("must-be-ingame"));
								return true;
							}
							arena.put("pos1", ((Player)sender).getLocation());
							//sender.sendMessage("i: " + i + " max: " + (pl.inProgressArenas.size()-1));
							pl.inProgressArenas.set(i, arena);
							sender.sendMessage(msgs.get("arenaeditor.set.pos1"));
						} else if(args[2].equalsIgnoreCase("pos2")) {
							if(!(sender instanceof Player)) {
								sender.sendMessage(msgs.get("must-be-ingame"));
								return true;
							}
							arena.put("pos2", ((Player)sender).getLocation());
							pl.inProgressArenas.set(i, arena);
							sender.sendMessage(msgs.get("arenaeditor.set.pos2"));
							
						} else if(args[2].equalsIgnoreCase("maxplayers")) {
							if(!(sender instanceof Player)) {
								sender.sendMessage(msgs.get("must-be-ingame"));
								return true;
							}
							if(args.length < 4) {
								sender.sendMessage(msgs.get("arenaeditor.more-args.maxplayers"));
								return true;
							}
							arena.put("maxplayers", Integer.valueOf(args[3]));
							pl.inProgressArenas.set(i, arena);
							sender.sendMessage(msgs.get("arenaeditor.set.maxplayers"));
							
						
						} else if(args[2].equalsIgnoreCase("save")) {
							if(arena.keySet().contains("pos1") && arena.keySet().contains("pos2") && arena.keySet().contains("startloc")) {
								ConfigurationSection cs = null;
								//cs = ConfigurationSection.createSection(arena.get("name").toString(), arena);
								cs = pl.arenafile.createSection("Arenas."+arena.get("name"));
								cs.set("startloc", arena.get("startloc"));
								cs.set("pos1", arena.get("pos1"));
								cs.set("pos2", arena.get("pos2"));
								
								if(arena.containsKey("lobby")) {
									cs.set("lobby", arena.get("lobby"));
								}
								if(arena.containsKey("maxplayers")) {
									cs.set("maxplayers", arena.get("maxplayers"));
								} else {
									cs.set("maxplayers", 16);
								}
								this.pl.arenafile.set("Arenas."+arena.get("name"), cs);
								try {
									pl.arenafile.save(pl.arenaFile);
									sender.sendMessage(msgs.get("arenaeditor.saved.success"));
								} catch (IOException e) {
									sender.sendMessage(msgs.get("arenaeditor.saved.fail"));
									e.printStackTrace();
								}
								pl.reloadTheConfig();
							} else {
								sender.sendMessage(msgs.get("arenaeditor.not-enough-positions"));
							}
						}
					} else {
						String ar = "&7&m       &r&9 Arena Setup Commands &7&m       ";
						ar += "\n&b/"+label+" arena " + arenaName + " pos1 &7- &9Set the first position. (Set at your feet)";
						ar += "\n&b/"+label+" arena " + arenaName + " pos2 &7- &9Set the second position. (Set at your feet)";
						ar += "\n&b/"+label+" arena " + arenaName + " maxplayers &7- (optional) &9Set the max number of players allowed in the arena. Can be bypassed with the permission atr.joinfull";
						ar += "\n&b/"+label+" arena " + arenaName + " startloc &7- &9Sets the starting point (where players are teleported when the game starts) (Set where you are standing)";
						ar += "\n&b/"+label+" arena " + arenaName + " lobby &7- (optional) &9Set lobby (where players are teleported while waiting for other players) (Set where you are standing)";
						ar += "\n&b/"+label+" arena " + arenaName + " save &7- &9Saves the arena and makes it available to play. (This should be the last step)";
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ar));
					}
				} else {
					sender.sendMessage(msgs.get("arenaeditor.must-put-a-name"));
				}
			} else if(args[0].equalsIgnoreCase("regdebug")) {
				if(!(sender instanceof Player)) return true;
				if(!sender.hasPermission("ajtntrun.debug")) return true;
				Player p = (Player) sender;
				ArenaPlayer player = pl.findPlayer(p);
				if(player == null) return true;
				Arena arena = player.getArena();
				Block start = p.getLocation().getBlock();
				int radius = 50;
				int d = 0;
				int i = 0;
				for(double x = start.getLocation().getX() - radius; x <= start.getLocation().getX() + radius; x++){
					if(i == 0) {
						i++;
					} else {
						i = 0;
						//continue;
					}
					for(double y = start.getLocation().getY() - radius; y <= start.getLocation().getY() + radius; y++){
						for(double z = start.getLocation().getZ() - radius; z <= start.getLocation().getZ() + radius; z++){
							Location loc = new Location(start.getWorld(), x+1.0, y, z+0.6);
							if(!arena.checkInBounds(loc)) continue;
							if(!loc.getBlock().getType().equals(Material.TNT)) continue;
							Bukkit.getScheduler().runTaskLater(pl, new Runnable() {
								public void run() {
									loc.setY(loc.getY()+2);
									//p.teleport(loc);
									arena.walk(loc);
								}
							}, d);
							if(i == 0) {
								d += 1;
							}
						}
					}
				}
				
			} else if(args[0].equalsIgnoreCase("update")) {
				if(!sender.hasPermission("ajtntrun.update")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				Updater.getInstance().downloadUpdate(sender);
			} else if(args[0].equalsIgnoreCase("stats")) {
				if(!(sender instanceof Player)) {
					sender.sendMessage(msgs.get("must-be-ingame"));
				}
				if(args.length == 2) {
					@SuppressWarnings("deprecation")
					OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
					if(player == null) {
						sender.sendMessage(msgs.get("stats.cannot-find-player"));
						return true;
					}
					sender.sendMessage(msgs.get("stats.header.other").replaceAll("\\{NAME\\}", player.getName())
							+ "\n" + msgs.get("stats.wins").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.WINS)+"")
							+ "\n" + msgs.get("stats.losses").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.LOSSES)+"")
							+ "\n" + msgs.get("stats.played").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.PLAYED)+"")
							);
				} else {
					Player player = (Player) sender;
					sender.sendMessage(msgs.get("stats.header.own")
							+ "\n" + msgs.get("stats.wins").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.WINS)+"")
							+ "\n" + msgs.get("stats.losses").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.LOSSES)+"")
							+ "\n" + msgs.get("stats.played").replaceAll("\\{NUM\\}", stats.get(player.getUniqueId(), StatType.PLAYED)+"")
							);
				}
			} else if(args[0].equalsIgnoreCase("createarena")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length == 2) {
					boolean found = false;
					for(Arena ar : pl.arenas) {
						if(ar.getName().equals(args[1])) {
							found = true;
							break;
						}
					}
					for(Map<String, Object> ar : pl.inProgressArenas) {
						if(ar.get("name").toString().equals(args[1])) {
							found = true;
							break;
						}
					}
					if(found) {
						sender.sendMessage(msgs.get("arenaeditor.create.already-exists"));
						return true;
					}
					Map<String, Object> arena = new HashMap<String, Object>();
					arena.put("name", args[1]);
					pl.inProgressArenas.add(arena);
					sender.sendMessage(msgs.get("arenaeditor.creating.success")
							.replaceAll("\\{CMD\\}", label)
							.replaceAll("\\{ARENA\\}", args[1]));
				} else {
					sender.sendMessage(msgs.get("arenaeditor.creating.must-pick-a-name"));
				}
			} else if(args[0].equalsIgnoreCase("join")) {
				if(args.length == 2) {
					if(pl.findPlayer((Player) sender) == null) {
						String arenaname = args[1];
						for(Arena arena : pl.arenas) {
							if(arena.getName().equalsIgnoreCase(arenaname)) {
								arena.addPlayer((Player) sender);
								return true;
							}
						}
						sender.sendMessage(msgs.get("joining.no-arena"));
					} else {
						sender.sendMessage(msgs.get("joining.already-in"));
					}
				} else {
					sender.sendMessage(msgs.get("joining.no-args"));
				}
			} else if(args[0].equalsIgnoreCase("leave")) {
				ArenaPlayer r = pl.findPlayer((Player) sender);
				if(r != null) {
					r.leave();
				} else {
					sender.sendMessage(msgs.get("leaving.not-in"));
				}
			} else if(args[0].equalsIgnoreCase("setlobby")) {
				if(sender.hasPermission("ajtr.setup")) {
					pl.arenafile.set("mainlobby", ((Player) sender).getLocation());
					try {
						pl.arenafile.save(pl.arenaFile);
						sender.sendMessage(msgs.get("mainlobby.success"));
					} catch (IOException e) {
						sender.sendMessage(msgs.get("mainlobby.fail"));
						e.printStackTrace();
					}
					pl.reloadTheConfig();
				} else {
					sender.sendMessage("noperm");
				}
			} else if(args[0].equalsIgnoreCase("editing")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				String ar = "&7&m       &r &9Edit Queue &7&m       ";
				int length = 0;
				for(Map<String, Object> arena : pl.inProgressArenas) {
					String pos1 = "&cpos1";
					String pos2 = "&cpos2";
					String startloc = "&cstartloc";
					String lobby = "&7lobby";
					String maxplayers = "&7maxplayers";
					if(arena.containsKey("pos1")) pos1 = "&apos1";
					if(arena.containsKey("pos2")) pos2 = "&apos2";
					if(arena.containsKey("startloc")) startloc = "&astartloc";
					if(arena.containsKey("lobby")) lobby = "&alobby";
					if(arena.containsKey("maxplayers")) lobby = "&amaxplayers";
					ar += "\n&d"+arena.get("name")+"&7 - "+pos1+" " + pos2 + " " + startloc + " " + lobby + " " + maxplayers;
					length++;
				}
				if(length < 1) {
					ar += "\n" + msgs.get("editing.none");
				}
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ar));
			} else if(args[0].equalsIgnoreCase("edit")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length == 2) {
					boolean found = false;
					for(Arena arena : pl.arenas) {
						if(arena.getName().equals(args[1])) {
							found = true;
							break;
						}
					}
					if(!found) {
						sender.sendMessage(msgs.get("arenaeditor.import.could-not-find"));
						return true;
					}
					
					Map<String, Object> arena = new HashMap<String, Object>();
					Set<String> keys = pl.arenafile.getConfigurationSection("Arenas."+args[1]).getKeys(false);
					arena.put("name", args[1]);
					for(String key : keys) {
						arena.put(key, pl.arenafile.get("Arenas."+args[1]+"."+key));
					}
					pl.inProgressArenas.add(arena);
					sender.sendMessage(msgs.get("arenaeditor.import.success")
							.replaceAll("\\{CMD\\}", label)
							.replaceAll("\\{ARENA\\}", args[1]));
					
				} else {
					sender.sendMessage(msgs.get("arenaeditor.import.not-enough-args"));
				}
			} else if(args[0].equalsIgnoreCase("list")) {
				String ar = "&7&m       &r &9Arenas &7&m       ";
				int length = 0;
				for(Arena arena : pl.arenas) {
					String name = arena.getName();
					int players = arena.getPlayerCount();
					String state = arena.getState().toString();
					ar += "\n&b&l"+name+" &r&7- &9"+state+" &7- &9" + players + " players";
					length++;
				}
				if(length < 1) {
					ar += "\n" + msgs.get("list.none");
				}
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ar));
			} else if(args[0].equalsIgnoreCase("reload")) {
				if(!sender.hasPermission("ajtr.reload")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				
				pl.reloadTheConfig();
				ItemManager.getInstance().reload();
				sender.sendMessage(msgs.get("reloaded"));
			} else if(args[0].equalsIgnoreCase("signs")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length > 1) {
					if(args[1].equalsIgnoreCase("add")) {
						if(!(sender instanceof Player)) {
							sender.sendMessage(msgs.get("must-be-ingame"));
							return true;
						}
						if(args.length > 2) {
							boolean found = false;
							Arena arena = null;
							Location target = ((Player) sender).getTargetBlock(null, 20).getLocation();
							if(!(target.getBlock().getState() instanceof Sign)) {
								sender.sendMessage(msgs.get("signs.add.not-a-sign"));
								return true;
							}
							for(Arena ar : pl.arenas) {
								if(ar.getName().equalsIgnoreCase(args[2])) {
									found = true;
									arena = ar;
									break;
								}
							}
							if(!found || arena == null) {
								sender.sendMessage(msgs.get("signs.add.could-not-find").replaceAll("\\{ARENA\\}", args[2]));
								return true;
							}
							
							for(ArenaSign sign : signs.getSigns()) {
								if(sign.sign.equals(target)) {
									sender.sendMessage(msgs.get("signs.add.already-added"));
									return true;
								}
							}
							
							SignManager.getInstance().addSign(arena, target);
							sender.sendMessage(msgs.get("signs.add.success").replaceAll("\\{ARENA\\}", arena.getName()));
						} else {
							sender.sendMessage(msgs.get("signs.add.need-an-arena"));
						}
					} else if(args[1].equalsIgnoreCase("remove")) {
						if(!(sender instanceof Player)) {
							sender.sendMessage(msgs.get("must-be-ingame"));
							return true;
						}
						Location target = ((Player) sender).getTargetBlockExact(20).getLocation();
						if(!(target.getBlock().getState() instanceof Sign)) {
							sender.sendMessage(msgs.get("signs.remove.not-a-sign"));
							return true;
						}
						for(ArenaSign sign : signs.getSigns()) {
							if(sign.sign.equals(target)) {
								signs.removeSign(sign);
								sender.sendMessage(msgs.get("signs.remove.success"));
								Sign sgn = (Sign) target.getBlock().getState();
								sgn.setLine(0, "");
								sgn.setLine(1, "");
								sgn.setLine(2, "");
								sgn.setLine(3, "");
								sgn.update();
								return true;
							}
						}
						sender.sendMessage(msgs.get("signs.remove.not-pl.arenasign"));
					} else if(args[1].equalsIgnoreCase("list")) {
						String list = "&7&m       &r &9Signs &7&m       ";
						int length = 0;
						for(ArenaSign sign : signs.getSigns()) {
							int x = sign.sign.getBlockX();
							int y = sign.sign.getBlockY();
							int z = sign.sign.getBlockZ();
							list += "\n&b("+x+", "+y+", "+z+") &3"+sign.arena.getName();
							length++;
						}
						if(length < 1) {
							list += "\n" + msgs.get("signs.list.none");
						}
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', list));
					} else {
						String hm = "&7&m       &r &9TNTRun Sign Commands &7&m       ";
						hm += "\n&b/"+label+" signs add &7- &9Make the sign you are looking at a tntrun sign.";
						hm += "\n&b/"+label+" signs remove &7- &9Remove the arena sign you are looking at.";
						hm += "\n&b/"+label+" signs list &7- &9List all arena signs";
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', hm));
					}
				} else {
					String hm = "&7&m       &r &9TNTRun Sign Commands &7&m       ";
					hm += "\n&b/"+label+" signs add &7- &9Make the sign you are looking at a tntrun sign.";
					hm += "\n&b/"+label+" signs remove &7- &9Remove the arena sign you are looking at.";
					hm += "\n&b/"+label+" signs list &7- &9List all arena signs";
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', hm));
				}
			} else if(args[0].equalsIgnoreCase("deletearena")) {
				if(!sender.hasPermission("ajtr.setup")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length < 2) {
					sender.sendMessage(msgs.get("arenaeditor.delete.no-arena"));
					return true;
				}
				Arena ar = null;
				for(Arena arena : pl.arenas) {
					if(arena.getName().equals(args[1])) {
						ar = arena;
						break;
					}
				}
				if(ar == null) {
					sender.sendMessage(msgs.get("arenaeditor.delete.arena-not-found"));
					return true;
				}
				pl.arenafile.set("Arenas."+ar.getName(), null);
				for(ArenaPlayer ply : ar.getPlayers()) {
					ar.kickPlayer(ply, "the arena is being deleted");
				}
				pl.arenas.remove(ar);
				try {
					pl.arenafile.save(pl.arenaFile);
					sender.sendMessage(msgs.get("arenaeditor.delete.success").replaceAll("\\{ARENA\\}", ar.getName()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(args[0].equalsIgnoreCase("start")) {
				if(!sender.hasPermission("ajtr.forcestart")) {
					sender.sendMessage(msgs.get("noperm"));
					return true;
				}
				if(args.length < 2) {
					sender.sendMessage(msgs.get("forcestart.fail.noarena"));
					return true;
				}
				Arena ar = null;
				for(Arena arena : pl.arenas) {
					if(arena.getName().equals(args[1])) {
						ar = arena;
						break;
					}
				}
				if(ar == null) {
					sender.sendMessage(msgs.get("forcestart.fail.arena-not-found").replaceAll("\\{ARENA\\}", args[1]));
					return true;
				}
				if(ar.getState().equals(ArenaState.WAITING) || ar.getState().equals(ArenaState.STARTING)) {
					ar.start();
					sender.sendMessage(msgs.get("forcestart.success").replaceAll("\\{ARENA\\}", ar.getName()));
				} else {
					sender.sendMessage(msgs.get("forcestart.fail.not-waiting"));
				}
				
			} else {
				sender.sendMessage(helpMessage(sender, label));
			}
		} else {
			sender.sendMessage(helpMessage(sender, label));
		}
		return true;
	}
	
	
	
	
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		if(args.length > 1) {
			List<String> npcs = new ArrayList<String>();
			if(!sender.hasPermission("ajtr.setup")) {
				npcs.add("arena");
				npcs.add("createarena");
				npcs.add("editing");
				npcs.add("edit");
				npcs.add("signs");
				npcs.add("setlobby");
				npcs.add("deletearena");
			}
			if(!sender.hasPermission("ajtr.reload")) {
				npcs.add("reload");
			}
			if(!sender.hasPermission("ajtr.forcestart")) {
				npcs.add("start");
			}
			
			if(npcs.contains(args[0].toLowerCase())) {
				return new ArrayList<String>();
			}
		}
		if(args.length == 1) {
			List<String> cmds = new ArrayList<String>();
			cmds.add("join");
			cmds.add("leave");
			cmds.add("list");
			if(sender.hasPermission("ajtr.setup")) {
				cmds.add("arena");
				cmds.add("createarena");
				cmds.add("editing");
				cmds.add("edit");
				cmds.add("signs");
				cmds.add("setlobby");
				cmds.add("deletearena");
			}
			if(sender.hasPermission("ajtr.reload")) {
				cmds.add("reload");
			}
			if(sender.hasPermission("ajtr.forcestart")) {
				cmds.add("start");
			}
			for(Iterator<String> i = cmds.iterator(); i.hasNext();) {
				String cmd = i.next();
				if(!cmd.startsWith(args[0])) {
					i.remove();
				}
			}
			return cmds;
		} else if(args.length == 2) {
			List<String> noargs = new ArrayList<String>();
			noargs.add("leave");
			noargs.add("list");
			noargs.add("editing");
			noargs.add("reload");
			noargs.add("createarena");
			if(args[0].equalsIgnoreCase("join") || args[0].equalsIgnoreCase("edit") || args[0].equalsIgnoreCase("deletearena") || args[0].equalsIgnoreCase("start")) {
				List<String> ar = new ArrayList<String>();
				for(Arena arena : pl.arenas) {
					if(arena.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
						ar.add(arena.getName());
					}
				}
				return ar;
			} else if(args[0].equalsIgnoreCase("arena")) {
				List<String> ar = new ArrayList<String>();
				for(Map<String, Object> arena : pl.inProgressArenas) {
					ar.add(arena.get("name").toString());
				}
				return ar;
			} else if(args[0].equalsIgnoreCase("signs")) {
				List<String> arg = new ArrayList<String>();
				arg.add("add");
				arg.add("remove");
				arg.add("list");
				for(Iterator<String> i = arg.iterator(); i.hasNext();) {
					String cmd = i.next();
					if(!cmd.startsWith(args[1])) {
						i.remove();
					}
				}
				return arg;
			} else if(noargs.contains(args[0].toLowerCase())) {
				return new ArrayList<String>();
			}
		} else if(args.length == 3) {
			if(args[0].equalsIgnoreCase("arena")) {
				List<String> points = new ArrayList<String>();
				points.add("pos1");
				points.add("pos2");
				points.add("startloc");
				points.add("lobby");
				points.add("maxplayers");
				points.add("save");
				for(Iterator<String> i = points.iterator(); i.hasNext();) {
					String point = i.next();
					if(!point.startsWith(args[2])) {
						i.remove();
					}
				}
				return points;
			} else if(args[0].equalsIgnoreCase("signs")) {
				if(args[1].equalsIgnoreCase("add")) {
					List<String> ar = new ArrayList<String>();
					for(Arena arena : pl.arenas) {
						ar.add(arena.getName());
					}
					List<String> removes = new ArrayList<String>();
					for(String n : ar) {
						if(!args[2].toLowerCase().startsWith(n.toLowerCase())) {
							removes.add(n);
						}
					}
					ar.removeAll(removes);
					return ar;
				} else {
					return new ArrayList<String>();
				}
			} else {
				return new ArrayList<String>();
			}
		} else {
			return new ArrayList<String>();
		}
		return new ArrayList<String>();
	}
	
	
	private String helpMessage(CommandSender sender, String label) {
		String hm = "&7&m       &r &9TNTRun Commands &7&m       ";
		hm += "\n&b/"+label+" join &7- &9Join an arena.";
		hm += "\n&b/"+label+" leave &7- &9Leave an arena.";
		hm += "\n&b/"+label+" list &7- &9List all active arenas.";
		if(sender.hasPermission("ajtr.setup")) {
			hm += "\n&b/"+label+" arena &7- &9Edit positions of an arena that is in the editing queue.";
			hm += "\n&b/"+label+" createarena &7- &9Create an arena.";
			hm += "\n&b/"+label+" editing &7- &9Lists arenas that are in the edit queue.";
			hm += "\n&b/"+label+" edit &7- &9Add an existing arena into the edit queue.";
			hm += "\n&b/"+label+" signs &7- &9Commands for managing arena signs.";
		}
		if(sender.hasPermission("ajtr.reload")) {
			hm += "\n&b/"+label+" reload &7- &9Reloads the config and messages.";
		}
		if(sender.hasPermission("ajtr.forcestart")) {
			hm += "\n&b/"+label+" start &7- &9Force-starts the arena.";
		}
		return ChatColor.translateAlternateColorCodes('&', hm);
	}
	
}
