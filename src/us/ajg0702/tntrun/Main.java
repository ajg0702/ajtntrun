package us.ajg0702.tntrun;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import us.ajg0702.tntrun.Placeholders;
import us.ajg0702.tntrun.Arena.Arena;
import us.ajg0702.tntrun.Arena.ArenaState;
import us.ajg0702.tntrun.Stats.Stats;
import us.ajg0702.tntrun.items.ItemManager;
import us.ajg0702.tntrun.signs.SignManager;
import us.ajg0702.tntrun.utils.InvManager;
import us.ajg0702.tntrun.utils.Updater;
import us.ajg0702.utils.spigot.Config;
import us.ajg0702.tntrun.Arena.ArenaPlayer;

public class Main extends JavaPlugin implements Listener {
	
	public Config config;
	
	public Stats stats;
	
	public List<Arena> arenas = new ArrayList<Arena>();
	
	List<Map<String, Object>> inProgressArenas = new ArrayList<Map<String, Object>>();
	
	public File arenaFile = new File(this.getDataFolder(), "arenas.yml");
	public YamlConfiguration arenafile;
	
	public Messages msgs;
	
	public boolean debug = false;
	
	
	Commands commands;
	
	
	
	
	public void onEnable() {
		msgs = new Messages(this);
		config = new Config(this);
		stats = Stats.getInstance(this);
		SignManager.getInstance(this);
		reloadTheConfig();
		this.getServer().getPluginManager().registerEvents(this, this);
		
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		
		Updater.getInstance(this);
		
		Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
			public void run() {
				for(Arena arena : arenas) {
					arena.checkAirPlayers();
				}
			}
		}, 20, 20);
		
		metrics = new Metrics(this);
		
		if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null){
			placeholders = new Placeholders(this);
            placeholders.register();
            this.papi = true;
		}
		
		
		
		commands = new Commands(this);
		this.getCommand("ajtntrun").setExecutor(commands);
		this.getCommand("ajtntrun").setTabCompleter(commands);
		
		Bukkit.getPluginManager().registerEvents(new Listeners(this), this);
		
		
		ItemManager.getInstance(this);
		
		
		Bukkit.getConsoleSender().sendMessage("§2ajTNTRun §av§2"+this.getDescription().getVersion() + "§a made by ajgeiss0702 has been enabled!");
	}
	public void onDisable() {
		List<Arena> ta = arenas;
		for(Arena arena : ta) {
			for(ArenaPlayer ply : arena.getPlayers()) {
				ply.getPlayer().teleport((Location) arenafile.get("mainlobby"));
				try {
					InvManager.restoreInventory(ply.getPlayer());
				} catch (IOException e) {
					getLogger().warning("Error encountered while trying to restore player's inventory:");
					e.printStackTrace();
				}
			}
			arena.saveRollbackBlocks();
		}
		Bukkit.getConsoleSender().sendMessage("§4ajTNTRun §cv§4"+this.getDescription().getVersion() + "§c made by ajgeiss0702 has been disabled!");
	}
	
	
	
	
	public void reloadTheConfig() {
		
		for(Iterator<Arena> iter = arenas.iterator(); iter.hasNext();) {
			Arena arena = iter.next();
			arena.changeState(ArenaState.INITIALIZING);
		}
		
		RewardCommands.getInstance(this);
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(this, new Runnable() {
			public void run() {
				arenafile = YamlConfiguration.loadConfiguration(arenaFile);
				msgs.reload();
				config.reload();
				arenas = new ArrayList<Arena>();
				ConfigurationSection section = arenafile.getConfigurationSection("Arenas");
				if(section != null) {
					int ra = -1;
					if(section.getKeys(false).size() > 0) {
						int min = 0;
						int max = section.getKeys(false).size()-1;
						ra = new Random().nextInt((max - min) + 1) + min;
					}
					int i = 0;
					for(String key : section.getKeys(false)) {
						if(config.get("enable-bungee") != null) {
							if((boolean) config.get("enable-bungee")) {
								//Bukkit.getLogger().info(i+"/"+ra);
								if(ra != i) {
									i++;
									continue;
								}
							}
						}
						//Bukkit.getLogger().info(key);
						ConfigurationSection sc = arenafile.getConfigurationSection("Arenas."+key);
						Arena arena = new Arena(sc);
						arenas.add(arena);
						i++;
					}
					if(config.get("enable-bungee") != null) {
						Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(Main.class), new Runnable() {
							public void run() {
								if((boolean) config.get("enable-bungee")) {
									for(Player player : Bukkit.getOnlinePlayers()) {
										if(arenas.size() > 0) {
											arenas.get(0).addPlayer(player);
										}
									}
								}
							}
						});
					}
				}
				
				
				debug = config.getBoolean("debug");
				
			}
		}, 5);
		SignManager.getInstance().reloadSigns();
		
	}
	
	Metrics metrics;
	
	Placeholders placeholders;
	boolean papi = false;
	
	
	
	
	
	public void setInArenaFile(String key, Object val) {
		arenafile.set(key, val);
		try {
			arenafile.save(arenaFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public ArenaPlayer findPlayer(Player player) {
		for(Arena arena : arenas) {
			ArenaPlayer plytest = arena.findPlayer(player);
			if(plytest != null) {
				return plytest;
			}
		}
		return null;
	}

	
}