package us.ajg0702.tntrun.utils;
import org.bukkit.Bukkit;

import us.ajg0702.tntrun.Main;

public class Debugger {
	public static void debug(String message) {
		Main pl = (Main) Bukkit.getPluginManager().getPlugin("ajTNTRun");
		if(!pl.debug) return;
		pl.getLogger().info("[Debugger] "+message);
	}
}
