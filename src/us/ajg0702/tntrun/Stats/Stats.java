package us.ajg0702.tntrun.Stats;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import us.ajg0702.tntrun.Main;
import us.ajg0702.tntrun.utils.Debugger;

public class Stats {
	
	Main plugin;
	File storageConfigFile;
	YamlConfiguration storageConfig;
	
	File scoresFile;
	YamlConfiguration scores;
	Connection conn;
	String tablename;
	
	String method = "none";
	
	static Stats INSTANCE = null;
	public static Stats getInstance(Main plugin) {
		if(INSTANCE == null) {
			INSTANCE = new Stats(plugin);
		}
		return INSTANCE;
	}
	public static Stats getInstance() {
		return INSTANCE;
	}

	private Stats(Main pl) {
		plugin = pl;
		storageConfigFile = new File(pl.getDataFolder(), "storage.yml");
		storageConfig = YamlConfiguration.loadConfiguration(storageConfigFile);
		
		checkStorageConfig();
		
		Debugger.debug("[storage] getString method: "+storageConfig.getString("method"));
		if(storageConfig.getString("method").equalsIgnoreCase("mysql")) {
			String ip = storageConfig.getString("mysql.ip");
			String username = storageConfig.getString("mysql.username");
			String password = storageConfig.getString("mysql.password");
			String database = storageConfig.getString("mysql.database");
			String table = storageConfig.getString("mysql.table");
			boolean useSSL = storageConfig.getBoolean("mysql.useSSL");
			try {
				initDatabase(ip, username, password, database, table, useSSL);
			} catch (Exception e) {
				pl.getLogger().warning("Could not connect to database! Switching to file storage. Error: " + e.getMessage());
				initYaml();
			}
		} else if(storageConfig.getString("method").equalsIgnoreCase("none")) {
			initNone();
		} else {
			initYaml();
		}
		Debugger.debug("[stats] Specified storage: "+storageConfig.getString("method")+" Selected storage: "+method);
	}
	
	private void checkStorageConfig() {
		Map<String, Object> v = new HashMap<String, Object>();
		v.put("method", "yaml");
		v.put("mysql.ip", "127.0.0.1:3306");
		v.put("mysql.username", "");
		v.put("mysql.password", "");
		v.put("mysql.database", "");
		v.put("mysql.table", "ajtntrun_stats");
		v.put("mysql.useSSL", false);
		
		boolean save = false;
		
		storageConfig.options().header("\n\nThis file tells the plugin where it\n"
				+ "should store player stats.\n\n"
				+ "The method option can either be 'yaml' or 'mysql'.\n"
				+ "If it is mysql, you must configure the mysql section below.\n\n ");
		for(String key : v.keySet()) {
			if(!storageConfig.isSet(key)) {
				storageConfig.set(key, v.get(key));
				save = true;
			}
		}
		if(save) {
			try {
				storageConfig.save(storageConfigFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void initYaml() {
		method = "yaml";
		scoresFile = new File(plugin.getDataFolder(), "stats.yml");
		scores = YamlConfiguration.loadConfiguration(scoresFile);
		scores.options().header("\n\nThis is the stats file.\nEveryone's stats are stored here.\n \n ");
	}
	
	private void initNone() {
		method = "none";
	}
	
	private void initDatabase(String ip, String username, String password, String database, String table, boolean useSSL) throws Exception {
		String url = "jdbc:mysql://"+ip+"/"+database+"?useSSL="+useSSL+"&autoReconnect=true";
		Class.forName("org.gjt.mm.mysql.Driver");
		tablename = table;
		conn = DriverManager.getConnection(url, username, password);
		conn.createStatement().executeUpdate("create table if not exists "+tablename+" "
				+ "(id VARCHAR(36) PRIMARY KEY, WINS INT(255), PLAYED int(255), LOSSES int(255), name VARCHAR(17))");
		try {
			conn.createStatement().executeUpdate("alter table "+tablename+" add PRIMARY KEY (`id`)");
		} catch(Exception e) {}
		method = "mysql";
	}
	
	
	public int get(UUID uuid, StatType stat) {
		if(method.equals("none")) {
			return 0;
		}
		if(method.equals("yaml")) {
			
			return Integer.valueOf(scores.getInt(uuid.toString() + "."+stat.toString(), 0));
			
		} else if(method.equals("mysql")) {
			
			try {
				ResultSet p = conn.createStatement().executeQuery("select "+stat.toString()+" from "+tablename+" where id='"+uuid.toString()+"'");
				int size = 0;
				if(p != null) {
					p.last();
					size = p.getRow();
				}
				if(size == 0) {
					return 0;
				}
				p.first();
				return p.getInt(1);
			} catch (SQLException e) {
				Bukkit.getLogger().severe("[ajTNTRun] An error occured when attempting to read from database:");
				e.printStackTrace();
				return 0;
			}
		}
		
		
		Bukkit.getLogger().severe("[ajTNTRun] getScore() could not find a method!");
		return -1;
	}
	
	public void add(UUID uuid, StatType stat, int add) {
		set(uuid, stat, get(uuid, stat)+add);
	}
	

	public void set(UUID uuid, StatType stat, int num) {
		if(method.equals("none")) return;
		if(method.equals("yaml")) {
			
			Bukkit.getScheduler().runTask(plugin, new Runnable() {
				public void run() {
					scores.set(uuid.toString()+"."+stat.toString(), num);
					try {
						scores.save(scoresFile);
					} catch (Exception e) {
						Bukkit.getLogger().severe("[ajTNTRun] Unable to save stats file:");
						e.printStackTrace();
					}
				}
			});
			
		} else if(method.equals("mysql")) {
			
			try {
				ResultSet r = conn.createStatement().executeQuery("select * from "+tablename+" where id='"+uuid.toString()+"'");
				int size = 0;
				if(r != null) {
					r.last();
					size = r.getRow();
				}
				if(size <= 0) {
					conn.createStatement().executeUpdate("insert into "+tablename+" (id, WINS, PLAYED, LOSSES, name) "
						+ "values ('"+uuid.toString()+"', 0, 0, 0, '"+Bukkit.getOfflinePlayer(uuid).getName()+"')");
				}
				conn.createStatement().executeUpdate("update "+tablename+" set " + stat.toString() + "=" + num + " where id='"+uuid.toString()+"'");
			} catch (SQLException e) {
				Bukkit.getLogger().severe("[ajTNTRun] Unable to set score for a player:");
				e.printStackTrace();
			}
			
		}
	}
	
	public String getName(UUID uuid) {
		if(method.equals("none")) return "Stats disabled";
		if(method.equals("yaml")) {
			
			return Bukkit.getOfflinePlayer(uuid).getName();
			
		} else if(method.equals("mysql")) {
			
			try {
				ResultSet r = conn.createStatement().executeQuery("select name from "+tablename+" where id='"+uuid.toString()+"'");
				int size = 0;
				if(r != null) {
					r.last();
					size = r.getRow();
				}
				if(size <= 0) {
					return null;
				}
				return r.getString("name");
			} catch (SQLException e) {
				Bukkit.getLogger().severe("[ajTNTRun] An error occured when attempting to read from database:");
				e.printStackTrace();
				return null;
			}
		}
		
		Bukkit.getLogger().severe("[ajTNTRun] getName() could not find a method!");
		return null;
	}
	
	public List<UUID> getPlayers() {
		if(method.equals("none")) {
			return new ArrayList<>();
		}
		if(method.equals("yaml")) {
			
			List<UUID> uuids = new ArrayList<UUID>();
			for(String key : scores.getKeys(false)) {
				uuids.add(UUID.fromString(key));
			}
			return uuids;
			
		} else if(method.equals("mysql")) {
			
			int size = 0;
			try {
				ResultSet r = conn.createStatement().executeQuery("select id from "+tablename);
				List<UUID> uuids = new ArrayList<UUID>();
				if(r != null) {
					r.last();
					size = r.getRow();
				}
				if(size > 0) {
					int i = size;
					r.first();
					while(i > 0) {
						//Bukkit.getLogger().info(i+"");
						uuids.add(UUID.fromString(r.getString(1)));
						i--;
						if(i > 0) {
							r.next();
						}
					}
				}
				return uuids;
			} catch (SQLException e) {
				Bukkit.getLogger().severe("[ajTNTRun] An error occured while trying to get list of ("+size+") scores:");
				e.printStackTrace();
				return null;
			}
			
		}
		Bukkit.getLogger().severe("[ajTNTRun] getPlayers() could not find a method!");
		return null;
	}
	
	
	public void updateName(UUID uuid) {
		updateName(uuid, Bukkit.getPlayer(uuid).getName());
	}
	
	public void updateName(UUID uuid, String newname) {
		if(method.equals("none")) {
			return;
		}
		if(method.equals("mysql")) {
			
			try {
				ResultSet r = conn.createStatement().executeQuery("select id from "+tablename+" where id='"+uuid.toString()+"'");
				int size = 0;
				if(r != null) {
					r.last();
					size = r.getRow();
				}
				
				if(size >= 0) {
					conn.createStatement().executeUpdate("update "+tablename+" set name='"+newname+"' where id='"+uuid.toString()+"'");
				}
			} catch (SQLException e) {
				Bukkit.getLogger().severe("[ajTNTRun] An error occured while trying to update name for player " + newname+":");
				e.printStackTrace();
			}
			
		}
	}
	
	/*
	public int migrate(String from) {
		if(method.equalsIgnoreCase(from)) {
			return 0;
		}
		if(from.equalsIgnoreCase("yaml")) {
			File sc = new File(plugin.getDataFolder(), "scores.yml");
			YamlConfiguration s = YamlConfiguration.loadConfiguration(sc);
			
			int count = 0;
			for(String key : s.getKeys(false)) {
				UUID uuid = UUID.fromString(key);
				setScore(uuid, s.getInt(key));
				count++;
			}
			sc = null;
			s = null;
			return count;
		} else if(from.equalsIgnoreCase("mysql")) {
			String ip = storageConfig.getString("mysql.ip");
			String username = storageConfig.getString("mysql.username");
			String password = storageConfig.getString("mysql.password");
			String database = storageConfig.getString("mysql.database");
			String table = storageConfig.getString("mysql.table");
			boolean useSSL = storageConfig.getBoolean("mysql.useSSL");
			String url = "jdbc:mysql://"+ip+"/"+database+"?useSSL="+useSSL;
			try {
				Class.forName("org.gjt.mm.mysql.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			Connection con;
			try {
				con = DriverManager.getConnection(url, username, password);
				con.createStatement().executeUpdate("create table if not exists "+table+" (id VARCHAR(36), score BIGINT(255), name VARCHAR(17))");
				
				ResultSet r = con.createStatement().executeQuery("select id,score from "+tablename);
				int size = 0;
				if(r != null) {
					r.last();
					size = r.getRow();
					
					int i = size;
					while(i > 0) {
						setScore(UUID.fromString(r.getString(1)), r.getInt(2));
						i--;
						r.next();
					}
					
				}
				return size;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	*/

}
