package us.ajg0702.tntrun;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import us.ajg0702.tntrun.Arena.Arena;
import us.ajg0702.tntrun.Arena.ArenaPlayer;
import us.ajg0702.tntrun.Arena.ArenaState;
import us.ajg0702.tntrun.Arena.PlayerState;
import us.ajg0702.tntrun.signs.ArenaSign;
import us.ajg0702.tntrun.signs.SignManager;
import us.ajg0702.utils.GenUtils;

public class Listeners implements Listener {
	
	Main pl;
	Messages msgs;
	public Listeners(Main pl) {
		this.pl = pl;
		this.msgs = pl.msgs;
	}
	
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if(pl.findPlayer((Player)e.getPlayer()) != null) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onItemclick(InventoryClickEvent e) {
		if(!(e.getWhoClicked() instanceof Player)) return;
		Player p = (Player) e.getWhoClicked();
		if(pl.findPlayer(p) == null) return;
		e.setCancelled(true);
	}
	
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		ArenaPlayer player = pl.findPlayer(e.getPlayer());
		if(player == null) return;
		Arena arena = player.getArena();
		arena.checkPlayerInBounds(e.getPlayer());
		if(arena.getState().equals(ArenaState.INGAME) && player.getState().equals(PlayerState.INGAME)) {
			arena.walk(player.getPlayer().getLocation());
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		ArenaPlayer ply = pl.findPlayer(player);
		if(ply != null) {
			e.setDeathMessage("");
			e.setDroppedExp(0);
			player.setFireTicks(0);
			e.setKeepInventory(true);
			e.setKeepLevel(true);
			player.setHealth(20.0);
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		ArenaPlayer ply = pl.findPlayer(e.getPlayer());
		if(ply != null) {
			ply.leave();
		}
		
		if(pl.config.get("enable-bungee") != null) {
			if((boolean) pl.config.get("enable-bungee")) {
				e.setQuitMessage("");
			}
		}
		if(pl.papi) {
			pl.placeholders.cleanCache();
		}
	}
	
	@EventHandler
	public void onHit(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			if(pl.findPlayer((Player)e.getEntity()) != null && (boolean)pl.config.get("block-pvp")) {
				e.setCancelled(true);
			}
		}
	}
	
	
	
	
	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent e) {
		if(pl.findPlayer(e.getPlayer()) != null) {
			//Bukkit.getLogger().info(e.getBlock().getType().toString());
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerPlaceBlock(BlockPlaceEvent e) {
		if(pl.findPlayer(e.getPlayer()) != null) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		ArenaPlayer ply = pl.findPlayer(e.getPlayer());
		if(ply != null) {
			if(e.getAction() == Action.PHYSICAL) {
				if(e.getClickedBlock().getType().toString().contains("_PLATE")) {
					e.setCancelled(true);
					/*Bukkit.getLogger().info(arena.state.toString() + " / " + arena.getState().toString());
					if(arena.state.equals(ArenaState.INGAME)) {
						e.setCancelled(true);
						arena.walk(e.getClickedBlock().getLocation());
					} else {
						e.setCancelled(true);
					}*/
				}
			} else if(System.currentTimeMillis() - ply.getJoined() > 100) {
				ply.getArena().clickItem(ply, e.getPlayer().getInventory().getHeldItemSlot());
				e.setCancelled(true);
			}
		} else {
			if(e.getClickedBlock() == null) return;
			if((e.getClickedBlock().getState() instanceof Sign) && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				Location loc = e.getClickedBlock().getLocation();
				for(ArenaSign sign : SignManager.getInstance().getSigns()) {
					if(sign.checkLoc(loc, e.getPlayer())) {
						e.setCancelled(true);
						break;
					}
				}
			}
		}
	}
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if(pl.config.get("enable-bungee") != null) {
			if((boolean) pl.config.get("enable-bungee") && pl.arenas.size() >= 1) {
				pl.arenas.get(0).addPlayer(e.getPlayer());
				e.setJoinMessage("");
			} else if((boolean) pl.config.get("enable-bungee")) {
				 Bukkit.getLogger().warning("[ajTNTRun] Bungee mode is enabled, but no pl.arenas are set up!");
			}
		}
	}
	
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		ArenaPlayer ply = pl.findPlayer(e.getPlayer());
		if(ply != null) {
			String blockcmds = pl.config.get("block-commands").toString();
			if(blockcmds != null) {
				if(Boolean.valueOf(blockcmds)) {
					String command = e.getMessage();
					String[] args = command.split(" ");
					String[] colon = args[0].split(":");
					if(colon.length > 1) {
						command = colon[1];
					} else {
						if(colon[0].split("/").length > 1) {
							command = colon[0].split("/")[1];
						}
					}
					
					List<String> allowedcmds = pl.config.getStringList("allowed-commands");
					if(!GenUtils.stringContains(allowedcmds, command) && !e.getPlayer().hasPermission("ajtr.bypasscommands")) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(msgs.get("command-blocked"));
					}
				}
			}
		}
	}
}
