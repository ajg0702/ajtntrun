plugins {
    java
    `maven-publish`
    id("com.github.johnrengelman.shadow").version("6.1.0")
}

repositories {
    mavenLocal()
    mavenCentral()

    maven { url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") }
    maven { url = uri("http://repo.extendedclip.com/content/repositories/placeholderapi/") }
    maven { url = uri("https://gitlab.com/api/v4/projects/19978391/packages/maven") }
}

dependencies {
    implementation("us.ajg0702:ajUtils:1.0.0")
    compileOnly("org.spigotmc:spigot-api:1.14.4-R0.1-SNAPSHOT")
    compileOnly("me.clip:placeholderapi:2.10.4")
}

group = "us.ajg0702"
version = "1.3.3"
description = "ajTNTRun"
java.sourceCompatibility = JavaVersion.VERSION_1_8

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}

tasks.shadowJar {
    relocate("us.ajg0702.utils", "us.ajg0702.tntrun.utils")
    archiveFileName.set("${baseName}-${version}.${extension}")
}

